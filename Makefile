cache-clear:
	@test -f bin/console && bin/console cache:clear --no-warmup --shop=$(shop) || rm -rf var/cache/*
.PHONY: cache-clear

cache-warmup: cache-clear
	@test -f bin/console && bin/console cache:warmup --shop=$(shop)
.PHONY: cache-warmup
