<?php

namespace App\Form;

use App\EventSubscriber\ThemeLoader;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ShopSettingsType extends AbstractType
{
    /**
     * @var ThemeLoader
     */
    private $themeLoader;

    /**
     * ShopSettingsType constructor.
     *
     * @param ThemeLoader $themeLoader
     */
    public function __construct(ThemeLoader $themeLoader)
    {
        $this->themeLoader = $themeLoader;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('theme', ChoiceType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ],
                'choices'  => array_combine($this->themeLoader->getThemes(), $this->themeLoader->getThemes()),
                'data'     => $this->themeLoader->getFrontTheme()
            ])
            ->add('admin_theme', ChoiceType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ],
                'choices'  => array_combine($this->themeLoader->getThemes(), $this->themeLoader->getThemes()),
                'data'     => $this->themeLoader->getAdminTheme()
            ])
            ->add('image_sizes', ShopSettingsImageSizesType::class)
        ;
    }
}
