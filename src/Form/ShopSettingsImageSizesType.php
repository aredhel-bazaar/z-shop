<?php

namespace App\Form;

use App\EventSubscriber\ShopLoader;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ShopSettingsImageSizesType extends AbstractType
{
    /**
     * @var ShopLoader
     */
    private $shopLoader;

    /**
     * ShopSettingsType constructor.
     *
     * @param ShopLoader  $shopLoader
     */
    public function __construct(ShopLoader $shopLoader)
    {
        $this->shopLoader = $shopLoader;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->shopLoader->getConfigItem('image_sizes') as $key => $sizes) {
            $builder
                ->add("size-{$key}-w", IntegerType::class, [
                    'label'       => "width",
                    'required'    => true,
                    'constraints' => [
                        new NotBlank()
                    ],
                    'data'        => $sizes['w'],
                    'mapped'      => false
                ])
                ->add("size-{$key}-h", IntegerType::class, [
                    'label'       => "height",
                    'required'    => true,
                    'constraints' => [
                        new NotBlank()
                    ],
                    'data'        => $sizes['h'],
                    'mapped'      => false
                ])
                ->add("size-{$key}-crop", ChoiceType::class, [
                    'label'       => "crop ?",
                    'required'    => true,
                    'choices'     => [
                        'No'  => false,
                        'Yes' => true
                    ],
                    'data'        => $sizes['crop'],
                    'mapped'      => false
                ])
                ->add($key, HiddenType::class)
            ;

            $builder->get($key)
                ->addModelTransformer(new CallbackTransformer(
                    /* Model to Norm */
                    function ($value) {
                        return $value;
                    },
                    /* Norm to Model */
                    function ($value) use($builder, $key) {
                        return [
                            'w'    => $_POST['shop_settings']['image_sizes']["size-{$key}-w"],
                            'h'    => $_POST['shop_settings']['image_sizes']["size-{$key}-h"],
                            'crop' => $_POST['shop_settings']['image_sizes']["size-{$key}-crop"],
                        ];
                    }
                ))
            ;
        }
    }


}
