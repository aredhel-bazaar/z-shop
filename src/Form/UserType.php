<?php

namespace App\Form;

use App\Entity\Shop;
use App\Entity\User;
use DateTimeZone;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'required' => true
            ])
            ->add('shop', EntityType::class, [
                'class'        => Shop::class,
                'choice_label' => 'domain',
                'required'     => true
            ])
            ->add('plainPassword', TextType::class, [
                'required' => !isset($options['data'])
            ])
            ->add('status', ChoiceType::class, [
                'choices'  => array_flip(User::STATUS_LABELS),
                'required' => true
            ])
            ->add('language', ChoiceType::class, [
                'choices'  => [
                    'fr' => 'fr',
                    'en' => 'en',
                ],
                'required' => true
            ])
            ->add('timezone', ChoiceType::class, [
                'choices'  => array_combine(DateTimeZone::listIdentifiers(), DateTimeZone::listIdentifiers()),
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
