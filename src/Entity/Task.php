<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Console\Command\Command;

/**
 * Class Task
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class Task
{
    const STATUS_PENDING = 0;
    const STATUS_RUNNING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_ERROR   = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected ?int $id = null;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="tasks")
     */
    protected ?Shop $shop = null;
    /**
     * @var string
     *
     * @ORM\Column(name="command", type="string", length=50)
     */
    protected string $command;
    /**
     * @var array
     *
     * @ORM\Column(name="payload", type="json")
     */
    protected array $payload = [];
    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    protected int $status = self::STATUS_PENDING;
    /**
     * @var int
     *
     * @ORM\Column(name="recurrence_count", type="integer", length=3)
     */
    protected int $recurrenceCount = 0;
    /**
     * @var \DateInterval
     *
     * @ORM\Column(name="recurrence_interval", type="dateinterval", nullable=true)
     */
    protected \DateInterval $recurrenceInterval;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="execution_datetime", type="datetime")
     */
    protected DateTime $executionDatetime;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="creation_datetime", type="datetime")
     */
    protected DateTime $creationDatetime;
    /**
     * @var array
     */
    protected array $commandInfos = [];

    public function __construct()
    {
        $this->creationDatetime = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Task
     */
    public function setId(int $id): Task
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     *
     * @return Task
     */
    public function setShop(Shop $shop): Task
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @param string $command
     *
     * @return Task
     */
    public function setCommand(string $command): Task
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return array
     */
    public function getCommandInfos()
    {
        return $this->commandInfos;
    }

    /**
     * @param array $infos
     *
     * @return $this
     */
    public function setCommandInfos(array $infos)
    {
        $this->commandInfos = $infos;

        return $this;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     *
     * @return Task
     */
    public function setPayload(array $payload): Task
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @return int
     */
    public function getRecurrenceCount(): int
    {
        return $this->recurrenceCount;
    }

    /**
     * @param int $recurrenceCount
     *
     * @return Task
     */
    public function setRecurrenceCount(int $recurrenceCount): Task
    {
        $this->recurrenceCount = $recurrenceCount;
        return $this;
    }

    /**
     * @return \DateInterval
     */
    public function getRecurrenceInterval(): \DateInterval
    {
        return $this->recurrenceInterval;
    }

    /**
     * @param \DateInterval $recurrenceInterval
     *
     * @return Task
     */
    public function setRecurrenceInterval(\DateInterval $recurrenceInterval): Task
    {
        $this->recurrenceInterval = $recurrenceInterval;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getExecutionDatetime(): DateTime
    {
        return $this->executionDatetime;
    }

    /**
     * @param DateTime $executionDatetime
     *
     * @return Task
     */
    public function setExecutionDatetime(DateTime $executionDatetime): Task
    {
        $this->executionDatetime = $executionDatetime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDatetime(): DateTime
    {
        return $this->creationDatetime;
    }

    /**
     * @param DateTime $creationDatetime
     *
     * @return Task
     */
    public function setCreationDatetime(DateTime $creationDatetime): Task
    {
        $this->creationDatetime = $creationDatetime;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return Task
     */
    public function setStatus(int $status): Task
    {
        $this->status = $status;
        return $this;
    }
}
