<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class Cart
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class Cart
{
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="doctrine.uuid_generator")
     */
    protected string $id;
    /**
     * @var Customer
     *
     * @ORM\OneToOne(targetEntity="Customer", inversedBy="cart")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected Customer $customer;
    /**
     * @var Product[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="carts")
     */
    protected $products;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    protected ?DateTime $creationDatetime;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="modification_datetime", type="datetime", nullable=true)
     */
    private ?DateTime $modificationDatetime;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->creationDatetime = new DateTime();
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     *
     * @return Cart
     */
    public function setCustomer(Customer $customer): Cart
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     *
     * @return Cart
     */
    public function setProducts(array $products): Cart
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDatetime(): DateTime
    {
        return $this->creationDatetime;
    }

    /**
     * @param DateTime $creationDatetime
     *
     * @return Cart
     */
    public function setCreationDatetime(DateTime $creationDatetime): Cart
    {
        $this->creationDatetime = $creationDatetime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModificationDatetime(): DateTime
    {
        return $this->modificationDatetime;
    }

    /**
     * @param DateTime $modificationDatetime
     *
     * @return Cart
     */
    public function setModificationDatetime(DateTime $modificationDatetime): Cart
    {
        $this->modificationDatetime = $modificationDatetime;
        return $this;
    }
}
