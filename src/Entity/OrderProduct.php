<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class Product
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class OrderProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected int $id;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="products")
     */
    protected Shop $order;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected string $name;
    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=3)
     */
    protected float $price;
    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Media", fetch="EAGER", inversedBy="products")
     * @ORM\JoinColumn(name="main_media", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected ?Media $mainMedia;
    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="products")
     */
    protected Brand $brand;
    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=30, nullable=true)
     */
    protected ?string $sku;
    /**
     * @var string
     *
     * @ORM\Column(name="npm", type="string", length=30, nullable=true)
     */
    protected ?string $mpn;
    /**
     * @var string
     *
     * @ORM\Column(name="ean", type="string", length=30, nullable=true)
     */
    protected ?string $ean;
    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", length=8)
     */
    protected int $quantity = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return self
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return Media
     */
    public function getMainMedia(): ?Media
    {
        return $this->mainMedia;
    }

    /**
     * @param Media $mainMedia
     *
     * @return self
     */
    public function setMainMedia(Media $mainMedia): self
    {
        $this->mainMedia = $mainMedia;
        return $this;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     *
     * @return self
     */
    public function setBrand(Brand $brand): self
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * @return string
     */
    public function getSku():? string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     *
     * @return self
     */
    public function setSku(string $sku): self
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return string
     */
    public function getMpn():? string
    {
        return $this->mpn;
    }

    /**
     * @param string $mpn
     *
     * @return self
     */
    public function setMpn(string $mpn): self
    {
        $this->mpn = $mpn;
        return $this;
    }

    /**
     * @return string
     */
    public function getEan():? string
    {
        return $this->ean;
    }

    /**
     * @param string $ean
     *
     * @return self
     */
    public function setEan(string $ean): self
    {
        $this->ean = $ean;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return OrderProduct
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return Shop
     */
    public function getOrder(): Shop
    {
        return $this->order;
    }

    /**
     * @param Shop $order
     *
     * @return OrderProduct
     */
    public function setOrder(Shop $order): self
    {
        $this->order = $order;

        return $this;
    }
}
