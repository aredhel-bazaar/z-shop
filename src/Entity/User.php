<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class User implements UserInterface, EquatableInterface
{
    public const STATUS_BLOCKED = 0;
    public const STATUS_WAITING = 1;
    public const STATUS_NORMAL  = 2;

    public const STATUS_LIST = [
        self::STATUS_BLOCKED,
        self::STATUS_WAITING,
        self::STATUS_NORMAL
    ];
    public const STATUS_LABELS = [
        self::STATUS_BLOCKED => 'blocked',
        self::STATUS_WAITING => 'waiting',
        self::STATUS_NORMAL  => 'normal'
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected int $id;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="users")
     */
    private ?Shop $shop = null;
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=100, unique=true)
     */
    protected string $username = '';
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100)
     */
    protected string $password = '';
    protected ?string $plainPassword = null;
    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=32)
     */
    protected string $salt;
    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    protected int $status = self::STATUS_NORMAL;
    /**
     * @var string
     *
     * @ORM\Column(name="recovery_key", type="string", length=64, nullable=true)
     */
    protected ?string $recoveryKey;
    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5)
     */
    protected string $language = '';
    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=20)
     */
    protected string $timezone = '';
    /**
     * @var Customer
     *
     * @ORM\OneToOne(targetEntity="Customer", mappedBy="user")
     */
    protected ?Customer $customer;

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return [
            'ROLE_ADMIN'
        ];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string|null The encoded password if any
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     *
     * @return User
     */
    public function setPlainPassword(string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        unset(
            $this->rawPassword
        );
    }

    /**
     * @param string|null $username
     *
     * @return $this
     */
    public function setUsername(?string $username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param string|null $password
     *
     * @return $this
     */
    public function setPassword(?string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string|null $salt
     *
     * @return $this
     */
    public function setSalt(?string $salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isEqualTo(UserInterface $user)
    {
        return $user->getUsername() === $this->getUsername() && $user->getPassword() === $this->getPassword();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return User
     */
    public function setStatus(int $status): User
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecoveryKey(): string
    {
        return $this->recoveryKey;
    }

    /**
     * @param string $recoveryKey
     *
     * @return User
     */
    public function setRecoveryKey(string $recoveryKey): User
    {
        $this->recoveryKey = $recoveryKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     *
     * @return User
     */
    public function setLanguage(string $language): User
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone(): string
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     *
     * @return User
     */
    public function setTimezone(string $timezone): User
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     *
     * @return User
     */
    public function setCustomer(Customer $customer): User
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     *
     * @return User
     */
    public function setShop(Shop $shop): User
    {
        $this->shop = $shop;
        return $this;
    }

    public function getStatusLabel()
    {
        switch ($this->getStatus()) {
            case self::STATUS_BLOCKED:
                return 'blocked';

            case self::STATUS_WAITING:
                return 'waiting';

            case self::STATUS_NORMAL:
                return 'normal';
        }

        return null;
    }
}
