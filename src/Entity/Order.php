<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class Order
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class Order
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="orders")
     */
    private Shop $shop;
    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="orders")
     */
    private Customer $customer;
    /**
     * @var Product[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="OrderProduct", mappedBy="order")
     */
    private $products;
    /**
     * @var float
     *
     * @ORM\Column(name="total_ttc", type="float", precision=3)
     */
    private float $totalTTC;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="creation_datetime", type="datetime")
     */
    private DateTime $creationDatetime;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="modification_datetime", type="datetime", nullable=true)
     */
    private ?DateTime $modificationDatetime = null;

    public function __construct()
    {
        $this->creationDatetime = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Order
     */
    public function setId(int $id): Order
    {
        $this->id = $id;
        return $this;
    }

    public function getShop(): Shop
    {
        return $this->shop;
    }

    public function setShop(Shop $shop): self
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     *
     * @return Order
     */
    public function setCustomer(Customer $customer): Order
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     *
     * @return Order
     */
    public function setProducts(array $products): Order
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDatetime(): DateTime
    {
        return $this->creationDatetime;
    }

    /**
     * @param DateTime $creationDatetime
     *
     * @return Order
     */
    public function setCreationDatetime(DateTime $creationDatetime): Order
    {
        $this->creationDatetime = $creationDatetime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModificationDatetime(): DateTime
    {
        return $this->modificationDatetime;
    }

    /**
     * @param DateTime $modificationDatetime
     *
     * @return Order
     */
    public function setModificationDatetime(DateTime $modificationDatetime): Order
    {
        $this->modificationDatetime = $modificationDatetime;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalTTC(): float
    {
        return $this->totalTTC;
    }

    /**
     * @param float $totalTTC
     *
     * @return Order
     */
    public function setTotalTTC(float $totalTTC): Order
    {
        $this->totalTTC = $totalTTC;
        return $this;
    }
}
