<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\PersistentCollection;

/**
 * Class Product
 * @package App\Entity
 *
 * @ORM\Entity()
 * @Table(name="product",indexes={
 *     @Index(name="sku_idx", columns={"sku"}),
 *     @Index(name="mpn_idx", columns={"mpn"}),
 *     @Index(name="ean_idx", columns={"ean"})
 * })
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected int $id;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="products")
     */
    protected Shop $shop;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected string $name;
    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=3)
     */
    protected float $price;
    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="declinations")
     */
    protected ?Product $parent;
    /**
     * @var Product[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="parent")
     */
    protected $declinations;
    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Media", fetch="EAGER", inversedBy="products")
     * @ORM\JoinColumn(name="main_media", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected ?Media $mainMedia;
    /**
     * @var Media[]|PersistentCollection
     *
     * @ORM\ManyToMany(targetEntity="Media")
     * @ORM\JoinTable(name="product_media",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", unique=false, onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="media_id", referencedColumnName="id", unique=false, onDelete="CASCADE")}
     * )
     */
    protected $medias;
    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="products")
     */
    protected Brand $brand;
    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     */
    protected ?Category $category = null;
    /**
     * @var Category[]|PersistentCollection
     *
     * @ORM\ManyToMany(targetEntity="Category")
     */
    protected $categories;
    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=30, nullable=true)
     */
    protected ?string $sku;
    /**
     * @var string
     *
     * @ORM\Column(name="mpn", type="string", length=30, nullable=true)
     */
    protected ?string $mpn;
    /**
     * @var string
     *
     * @ORM\Column(name="ean", type="string", length=30, nullable=true)
     */
    protected ?string $ean;
    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer", length=8)
     */
    protected int $stock = 0;
    /**
     * @var Cart[]|PersistentCollection
     *
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="products", fetch="EXTRA_LAZY")
     */
    protected $carts;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    protected ?DateTime $creationDatetime;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="modification_datetime", type="datetime", nullable=true)
     */
    private ?DateTime $modificationDatetime;

    public function __construct()
    {
        $this->creationDatetime = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Product
     */
    public function setId(int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return Product
     */
    public function setPrice(float $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return Product[]
     */
    public function getDeclinations(): array
    {
        return $this->declinations;
    }

    /**
     * @param Product[] $declinations
     *
     * @return Product
     */
    public function setDeclinations(array $declinations): Product
    {
        $this->declinations = $declinations;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     *
     * @return Product
     */
    public function setShop(Shop $shop): Product
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return Product
     */
    public function getParent(): Product
    {
        return $this->parent;
    }

    /**
     * @param Product $parent
     *
     * @return Product
     */
    public function setParent(Product $parent): Product
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Media
     */
    public function getMainMedia(): ?Media
    {
        return $this->mainMedia;
    }

    /**
     * @param Media $mainMedia
     *
     * @return Product
     */
    public function setMainMedia(Media $mainMedia): Product
    {
        $this->mainMedia = $mainMedia;
        return $this;
    }

    /**
     * @return Media[]
     */
    public function getMedias(): PersistentCollection
    {
        return $this->medias;
    }

    /**
     * @param Media[] $medias
     *
     * @return Product
     */
    public function setMedias(array $medias): Product
    {
        $this->medias = $medias;
        return $this;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     *
     * @return Product
     */
    public function setBrand(Brand $brand): Product
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * @return string
     */
    public function getSku():? string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     *
     * @return Product
     */
    public function setSku(string $sku): Product
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return string
     */
    public function getMpn():? string
    {
        return $this->mpn;
    }

    /**
     * @param string $mpn
     *
     * @return Product
     */
    public function setMpn(string $mpn): Product
    {
        $this->mpn = $mpn;
        return $this;
    }

    /**
     * @return string
     */
    public function getEan():? string
    {
        return $this->ean;
    }

    /**
     * @param string $ean
     *
     * @return Product
     */
    public function setEan(string $ean): Product
    {
        $this->ean = $ean;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     *
     * @return Product
     */
    public function setStock(int $stock): Product
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDatetime(): DateTime
    {
        return $this->creationDatetime;
    }

    /**
     * @param DateTime $creationDatetime
     *
     * @return Product
     */
    public function setCreationDatetime(DateTime $creationDatetime): Product
    {
        $this->creationDatetime = $creationDatetime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModificationDatetime(): DateTime
    {
        return $this->modificationDatetime;
    }

    /**
     * @param DateTime $modificationDatetime
     *
     * @return Product
     */
    public function setModificationDatetime(DateTime $modificationDatetime): Product
    {
        $this->modificationDatetime = $modificationDatetime;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     *
     * @return Product
     */
    public function setCategory(?Category $category): Product
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Category[]|PersistentCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category[]|PersistentCollection $categories
     *
     * @return Product
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return Cart[]|PersistentCollection
     */
    public function getCarts()
    {
        return $this->carts;
    }

    /**
     * @param Cart[]|PersistentCollection $carts
     *
     * @return Product
     */
    public function setCarts($carts)
    {
        $this->carts = $carts;
        return $this;
    }
}
