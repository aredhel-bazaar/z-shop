<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class Customer
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected int $id;
    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="customer")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected User $user;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=192)
     */
    protected string $email;
    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=60)
     */
    protected string $firstName;
    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=60)
     */
    protected string $lastName;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="customers")
     */
    protected Shop $shop;
    /**
     * @var Cart
     *
     * @ORM\OneToOne(targetEntity="Cart", mappedBy="customer")
     */
    protected ?Cart $cart = null;
    /**
     * @var Order[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Order", mappedBy="customer")
     */
    private $orders;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="creation_datetime", type="datetime")
     */
    private DateTime $creationDatetime;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="modification_datetime", type="datetime", nullable=true)
     */
    private ?DateTime $modificationDatetime = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Customer
     */
    public function setId(int $id): Customer
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Customer
     */
    public function setUser(User $user): Customer
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return Customer
     */
    public function setEmail(string $email): Customer
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return Customer
     */
    public function setFirstName(string $firstName): Customer
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return Customer
     */
    public function setLastName(string $lastName): Customer
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return Cart
     */
    public function getCart(): Cart
    {
        return $this->cart;
    }

    /**
     * @param Cart $cart
     *
     * @return Customer
     */
    public function setCart(Cart $cart): Customer
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return Order[]
     */
    public function getOrders(): array
    {
        return $this->orders;
    }

    /**
     * @param Order[] $orders
     *
     * @return Customer
     */
    public function setOrders(array $orders): Customer
    {
        $this->orders = $orders;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDatetime(): DateTime
    {
        return $this->creationDatetime;
    }

    /**
     * @param DateTime $creationDatetime
     *
     * @return Customer
     */
    public function setCreationDatetime(DateTime $creationDatetime): Customer
    {
        $this->creationDatetime = $creationDatetime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModificationDatetime(): DateTime
    {
        return $this->modificationDatetime;
    }

    /**
     * @param DateTime $modificationDatetime
     *
     * @return Customer
     */
    public function setModificationDatetime(DateTime $modificationDatetime): Customer
    {
        $this->modificationDatetime = $modificationDatetime;
        return $this;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }
}
