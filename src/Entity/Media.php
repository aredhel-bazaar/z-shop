<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Media
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class Media implements \JsonSerializable
{
    public const STATUS_NORMAL  = 0;
    public const STATUS_DELETED = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     */
    private Shop $shop;
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    private User $user;
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private string $path;
    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private string $filename;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private string $name;
    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    private int $status = self::STATUS_NORMAL;
    /**
     * @var Product[]
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="mainMedia", fetch="EXTRA_LAZY")
     */
    private $products;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    protected ?DateTime $creationDatetime;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="modification_datetime", type="datetime", nullable=true)
     */
    private ?DateTime $modificationDatetime = null;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->creationDatetime = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Media
     */
    public function setId(int $id): Media
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     *
     * @return Media
     */
    public function setShop(Shop $shop): Media
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return Media
     */
    public function setPath(string $path): Media
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     *
     * @return Media
     */
    public function setFilename(string $filename): Media
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Media
     */
    public function setName(string $name): Media
    {
        $this->name = $name;
        return $this;
    }

    public function getFullPath()
    {
        return $this->getPath() . ($this->getPath() === '/' ? '' : '/') . $this->getFilename();
    }

    /**
     * @return DateTime
     */
    public function getCreationDatetime(): ?DateTime
    {
        return $this->creationDatetime;
    }

    /**
     * @param DateTime $creationDatetime
     *
     * @return Media
     */
    public function setCreationDatetime(DateTime $creationDatetime): Media
    {
        $this->creationDatetime = $creationDatetime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModificationDatetime(): ?DateTime
    {
        return $this->modificationDatetime;
    }

    /**
     * @param DateTime $modificationDatetime
     *
     * @return Media
     */
    public function setModificationDatetime(DateTime $modificationDatetime): Media
    {
        $this->modificationDatetime = $modificationDatetime;
        return $this;
    }

    public function getThumbnail(string $size = 'o')
    {
        return $this->getPath() . (!empty($this->getPath()) && substr($this->getPath(), -1) !== '/' ? '/' : '') . '.' . pathinfo($this->getFilename(), PATHINFO_FILENAME) . '-' . $size . '.' . pathinfo($this->getFilename(), PATHINFO_EXTENSION);
    }

    public function getThumbnails(array $sizes)
    {
        $thumbnails = [];
        foreach ($sizes as $size) {
            $thumbnails[$size] = $this->getThumbnail($size);
        }

        return $thumbnails;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Media
     */
    public function setUser(User $user): Media
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     *
     * @return Media
     */
    public function setProducts(array $products): Media
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Media
     */
    public function setStatus(int $status): Media {
        $this->status = $status;
        return $this;
    }

    public function supportsThumbnail()
    {
        return in_array(mb_strtolower(pathinfo($this->getFilename(), PATHINFO_EXTENSION)), [
            'jpg',
            'jpeg',
            'png',
            'gif'
        ]);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id'                   => $this->getId(),
            'filename'             => $this->getFilename(),
            'fullpath'             => $this->getFullPath(),
            'creationDatetime'     => $this->getCreationDatetime()->format('Y-m-d H:i'),
            'modificationDatetime' => $this->getModificationDatetime() ? $this->getModificationDatetime()->format('Y-m-d H:i') : null,
            'supportsThumbnail'    => $this->supportsThumbnail()
        ];
    }
}
