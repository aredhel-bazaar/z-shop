<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Setting
 * @package App\Entity
 *
 * @ORM\Entity
 */
class Setting
{
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="string", length=30)
     */
    private string $id;
    /**
     * @var Shop
     *
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="settings")
     */
    private Shop $shop;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30)
     */
    private string $type;
    /**
     * @var mixed
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Setting
     */
    public function setId(string $id): Setting
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     *
     * @return Setting
     */
    public function setShop(Shop $shop): Setting
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Setting
     */
    public function setType(string $type): Setting
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        switch ($this->getType()) {
            case 'json':
                $value = json_decode($this->value);
                break;

            default:
                $value = $this->value;
        }
        return $value;
    }

    /**
     * @param mixed $value
     *
     * @return Setting
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
