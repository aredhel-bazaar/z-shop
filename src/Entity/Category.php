<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected int $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected string $name;
    /**
     * @var integer
     * 
     * @ORM\Column(name="lft", type="integer")
     */
    protected int $lft;
    /**
     * @var integer
     * 
     * @ORM\Column(name="rgt", type="integer")
     */
    protected int $rgt;
    /**
     * @var Media|null
     *
     * @ORM\ManyToOne(targetEntity="Media", fetch="EAGER")
     * @ORM\JoinColumn(name="main_media", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected ?Media $media;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected string $description;
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    protected string $path;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", fetch="EXTRA_LAZY")
     */
    protected Shop $shop;
    /**
     * @var Product[]
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category")
     */
    protected $products;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Category
     */
    public function setName(string $name): Category
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Media|null
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * @param Media|null $media
     *
     * @return Category
     */
    public function setMedia(?Media $media): Category
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Category
     */
    public function setDescription(string $description): Category
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     *
     * @return Category
     */
    public function setProducts(array $products): Category
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     *
     * @return Category
     */
    public function setShop(Shop $shop): Category
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return Category
     */
    public function setPath(string $path): Category
    {
        $this->path = $path;
        return $this;
    }
}