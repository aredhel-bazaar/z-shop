<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class UserAgent
 * @package App\Entity
 *
 * @ORM\Entity
 */
class UserAgent
{
    /**
     * @var object
     */
    private ?object $decodedData = null;
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="uid", type="string", length=64)
     */
    private string $uid;
    /**
     * @var string
     *
     * @ORM\Column(name="data", type="json")
     */
    private $data;
    
    const MIME_TYPE_AUDIO_MP3    = 'audio/mp3';
    const MIME_TYPE_AUDIO_AAC    = 'audio/aac';
    const MIME_TYPE_AUDIO_FLAC   = 'audio/flac';
    const MIME_TYPE_AUDIO_VORBIS = 'audio/vorbis';
    
    const MIME_TYPES = [
        self::MIME_TYPE_AUDIO_MP3,
        self::MIME_TYPE_AUDIO_AAC,
        self::MIME_TYPE_AUDIO_FLAC,
        self::MIME_TYPE_AUDIO_VORBIS,
    ];
    
    
    
    /**
     * UserAgent constructor.
     *
     * @param object $data
     */
    public function __construct(object $data)
    {
        if (!is_null($data)) {
            $this->decodedData = $data;
            $this->data        = json_encode($this->decodedData);
        }
    }

    public static function calcUid($httpUserAgent)
    {
        return hash('sha1', $httpUserAgent);
    }
    
    /**
     * @param string $format
     *
     * @return integer
     */
    public function supportsMimeType(string $format)
    {
        switch ($format) {
            case 'audio/mpeg':
            case self::MIME_TYPE_AUDIO_MP3:
                return (int) (
                    $this->isDesktop() && $this->getBrowser()->name == 'Firefox' && $this->getBrowser()->version >= 22 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'Chrome' && $this->getBrowser()->version >= 4 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'IE' && $this->getBrowser()->version >= 9 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'Edge'
                );
                break;
                
            case self::MIME_TYPE_AUDIO_VORBIS:
            case 'audio/ogg':
                return (int) (
                    $this->isDesktop() && $this->getBrowser()->name == 'Firefox' && $this->getBrowser()->version >= 3.5 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'Chrome' && $this->getBrowser()->version >= 4
                );
                break;
                
            case self::MIME_TYPE_AUDIO_AAC:
                return (
                    $this->isDesktop() && $this->getBrowser()->name == 'Chrome' && $this->getBrowser()->version >= 12 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'IE' && $this->getBrowser()->version >= 9 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'Edge'
                );
                break;
                
            case self::MIME_TYPE_AUDIO_FLAC:
            case 'audio/x-flac':
                return (int) (
                    $this->isDesktop() && $this->getBrowser()->name == 'Firefox' && $this->getBrowser()->version >= 51 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'Chrome' && $this->getBrowser()->version >= 56 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'Edge' && $this->getBrowser()->version >= 16
                );
                break;

            case 'video/x-flv':
                return 2;
                break;

            case 'video/mp4':
            case 'video/mpeg':
                return (int) (
                    $this->isDesktop() && $this->getBrowser()->name == 'Firefox' && $this->getBrowser()->version >= 35 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'Chrome' && $this->getBrowser()->version >= 4 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'IE' && $this->getBrowser()->version >= 9 ||
                    $this->isDesktop() && $this->getBrowser()->name == 'Edge'
                );
                break;

            case 'video/x-msvideo':
            case 'video/x-ms-asf':
                return 0;
                break;
        }
        
        return 0;
    }
    
    
    
    /**
     * @return array
     */
    public function getSupportedMimeTypes()
    {
        $supportedMimeTypes = [];
        foreach (self::MIME_TYPES as $mimeType) {
            if ($this->supportsMimeType($mimeType)) {
                $supportedMimeTypes[] = $mimeType;
            }
        }
        
        return $supportedMimeTypes;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     *
     * @return self
     */
    public function setData(string $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     *
     * @return UserAgent
     */
    public function setUid(string $uid): UserAgent
    {
        $this->uid = self::calcUid($uid);
        return $this;
    }

    public function getDecodedData()
    {
        if (is_null($this->decodedData)) {
            $this->decodedData = json_decode($this->data);
        }

        return $this->decodedData;
    }

    public function isMobile(): bool
    {
        return $this->getDecodedData()->device->is_mobile_device;
    }

    public function isTablet(): bool
    {
        return $this->getDecodedData()->device->type === 'tablet';
    }

    public function isDesktop(): bool
    {
        return $this->getDecodedData()->device->type === 'desktop';
    }

    public function isCrawler(): bool
    {
        return $this->getDecodedData()->crawler->is_crawler;
    }

    public function getBrowser()
    {
        return $this->getDecodedData()->browser;
    }

    public function getOs()
    {
        return $this->getDecodedData()->os;
    }

    public function getCrawler()
    {
        return $this->getDecodedData()->crawler;
    }
}
