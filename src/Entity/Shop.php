<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Security\Core\Authentication\RememberMe\PersistentToken;

/**
 * Class Shop
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class Shop
{
    public const STATUS_CLOSED = 0;
    public const STATUS_OPENED = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;
    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=50)
     */
    private string $domain;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private string $name;
    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    private int $status;
    /**
     * @var boolean
     *
     * @ORM\Column(name="secure", type="boolean")
     */
    private bool $secure;
    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="shops")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Shop $parent;
    /**
     * @var Product[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="shop", fetch="EXTRA_LAZY")
     */
    private $products;
    /**
     * @var Order[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Order", mappedBy="shop", fetch="EXTRA_LAZY")
     */
    private $orders;
    /**
     * @var User[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="shop", fetch="EXTRA_LAZY")
     */
    private $users;
    /**
     * @var Task[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Task", mappedBy="shop")
     */
    private $tasks;
    /**
     * @var Shop[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Shop", mappedBy="parent")
     */
    private $shops;
    /**
     * @var Customer[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Customer", mappedBy="shop")
     */
    private $customers;
    /**
     * @var Brand[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Brand", mappedBy="shop")
     */
    private $brands;
    /**
     * @var float
     *
     * @ORM\Column(name="disk_usage", type="float")
     */
    private float $diskUsage;
    /**
     * @var Setting[]|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="Setting", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $settings;
    /**
     * @var array
     */
    private ?array $config;

    public function __construct()
    {
        $this->settings = new ArrayCollection();
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        return $this->status === self::STATUS_CLOSED;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Shop
     */
    public function setId(string $id): Shop
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusLabel()
    {
        switch ($this->getStatus()) {
            case self::STATUS_CLOSED:
                return 'closed';

            case self::STATUS_OPENED:
                return 'opened';
        }

        return null;
    }

    /**
     * @param int $status
     *
     * @return Shop
     */
    public function setStatus(int $status): Shop
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): PersistentCollection
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     *
     * @return Shop
     */
    public function setProducts(array $products): Shop
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     *
     * @return Shop
     */
    public function setDomain(string $domain): Shop
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Shop
     */
    public function setName(string $name): Shop
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getParent(): ?Shop
    {
        return $this->parent;
    }

    /**
     * @param Shop $parent
     *
     * @return Shop
     */
    public function setParent(Shop $parent): Shop
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getUsers(): array|PersistentCollection
    {
        return $this->users;
    }

    /**
     * @param User[] $users
     *
     * @return Shop
     */
    public function setUsers(array $users): Shop
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return Task[]
     */
    public function getTasks(): array|PersistentCollection
    {
        return $this->tasks;
    }

    /**
     * @param Task[] $tasks
     *
     * @return Shop
     */
    public function setTasks(array $tasks): Shop
    {
        $this->tasks = $tasks;
        return $this;
    }

    /**
     * @return Task[]
     */
    public function getShops(): array|PersistentCollection
    {
        return $this->shops;
    }

    /**
     * @param Task[] $shops
     *
     * @return Shop
     */
    public function setShops(array $shops): Shop
    {
        $this->shops = $shops;
        return $this;
    }

    /**
     * @return Brand[]
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * @param Brand[] $brands
     *
     * @return Shop
     */
    public function setBrands(array $brands): Shop
    {
        $this->brands = $brands;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiskUsage(): ?float
    {
        return $this->diskUsage;
    }

    /**
     * @param float $diskUsage
     *
     * @return Shop
     */
    public function setDiskUsage(float $diskUsage): Shop
    {
        $this->diskUsage = $diskUsage;
        return $this;
    }

    /**
     * @return Setting[]
     */
    public function getSettings()
    {
        return $this->settings;
    }
    
    public function addSetting(Setting $setting): Shop 
    {
        $setting->setShop($this);
        $this->settings[] = $setting;

        return $this;
    }
    public function setSetting(Setting $setting): Shop 
    {
        foreach ($this->settings as $key => $storedSetting) {
            if ($storedSetting->getId() === $setting->getId()) {
                $this->settings[$key] = $setting;
                break;
            }
        }

        return $this;
    }
    public function removeSetting(Setting $setting): Shop 
    {
        foreach ($this->settings as $key => $storedSetting) {
            if ($storedSetting->getId() === $setting->getId()) {
                unset($this->settings[$key]);
                break;
            }
        }

        return $this;
    }
    /**
     * @param Setting[] $settings
     *
     * @return Shop
     */
    public function setSettings(array $settings): Shop
    {
        $this->settings = $settings;
        return $this;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSecure(): bool
    {
        return $this->secure;
    }

    /**
     * @param bool $secure
     *
     * @return Shop
     */
    public function setSecure(bool $secure): self
    {
        $this->secure = $secure;

        return $this;
    }

    public function getUrl()
    {
        $secure = $this->isSecure() ? 's' : '';
        return "http{$secure}://{$this->getDomain()}/";
    }

    /**
     * @return Order[]
     */
    public function getOrders(): array|PersistentCollection
    {
        return $this->orders;
    }

    /**
     * @param Order[] $orders
     *
     * @return Shop
     */
    public function setOrders(array $orders): self
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * Get the value of customers
     *
     * @return  Customer[]|PersistentCollection
     */ 
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Set the value of customers
     *
     * @param  Customer[]|PersistentCollection  $customers
     *
     * @return  self
     */ 
    public function setCustomers($customers)
    {
        $this->customers = $customers;

        return $this;
    }
}
