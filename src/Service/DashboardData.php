<?php

namespace App\Service;

use ArrayObject;

class DashboardData
{
    const SEVEN_DAYS = 100800;
    const THREE_DAYS = 33600;
    const ONE_DAY = 14400;

    /**
     * @var array
     */
    private $data;
    /**
     * @var Cloudflare
     */
    private $cloudflare;

    public function __construct(Cloudflare $cloudflare)
    {
        $this->cloudflare = $cloudflare;
    }

    public function getData(int $config)
    {
        if (is_null($this->data)) {
            $cloudflareData = $this->cloudflare->getDashboard(-($config/10), 0);

            $requests = [];
            $uniques  = [];
            if (!empty($cloudflareData)) {
                foreach ($cloudflareData->timeseries as $timeserie) {
                    $requests[] = [
                        'since'  => $timeserie->since,
                        'until'  => $timeserie->until,
                        'value'  => $timeserie->requests->all,
                    ];
                    $uniques[] = [
                        'since'  => $timeserie->since,
                        'until'  => $timeserie->until,
                        'value'  => $timeserie->uniques->all,
                    ];
                }
            }

            $this->data = [
                'timeseries' => new ArrayObject([
                    'requests' => $requests,
                    'uniques'  => $uniques,
                ]),
                'totals'     => new ArrayObject([
                    'since'    => $cloudflareData->totals->since ?? (time()-$config),
                    'until'    => $cloudflareData->totals->until ?? (time()),
                    'requests' => $cloudflareData->totals->requests ?? ['all' => 0],
                    'uniques'  => $cloudflareData->totals->uniques ?? ['all' => 0],
                    'orders'   => []
                ])
            ];
        }

        return $this->data;
    }

    public function buildCache()
    {
        $this->cloudflare->buildCache();
    }
}
