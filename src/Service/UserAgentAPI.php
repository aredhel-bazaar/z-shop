<?php

namespace App\Service;

use App\Entity\UserAgent;
use Doctrine\ORM\EntityManagerInterface;

class UserAgentAPI
{
    const API_KEY          = '1c788d3b911115dbcfc155cc1e16fa78';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UserAgentAPI constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return UserAgent
     * @throws \Exception
     */
    public function getUserAgent()
    {
        $userAgent = $this->matchUserAgent();
        if (!$userAgent) {
            $userAgent = $this->queryUserAgent();
            $this->saveUserAgent($userAgent);
        }

        return $userAgent;
    }


    /**
     * @param UserAgent $userAgent
     */
    public function saveUserAgent(UserAgent $userAgent)
    {
        $this->entityManager->persist($userAgent);
        $this->entityManager->flush();
    }

    private function matchUserAgent()
    {
        return $this->entityManager->getRepository(UserAgent::class)->findOneBy(['uid' => UserAgent::calcUid($_SERVER['HTTP_USER_AGENT'])]);
    }

    private function queryUserAgent()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://api.userstack.com/detect?access_key='.self::API_KEY.'&ua='.curl_escape($ch, $_SERVER['HTTP_USER_AGENT']).'&output=json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($ch);

        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);

        $decodedOutput = json_decode($output);

        if (empty($decodedOutput) || $curl_errno != 0) {
            throw new \Exception("Unable to contact the API. ".$curl_error);
        }

        $userAgent = new UserAgent($decodedOutput);
        $userAgent->setUid($_SERVER['HTTP_USER_AGENT']);

        return $userAgent;
    }
}
