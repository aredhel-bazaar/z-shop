<?php

namespace App\Service;

use Imagick;
use ImagickPixel;
use Symfony\Component\HttpFoundation\File\File;

class MagickManipulator
{
    private int $width;
    private int $height;

    private ?int $maxHeight = null;
    private ?int $maxWidth  = null;

    private ?int $newHeight = null;
    private ?int $newWidth  = null;
    private int $x;
    private int $y;

    private string   $projectDir;
    private bool     $allowCrop;
    private Imagick  $imagick;
    private string   $filepath;
    private bool     $optimizeSize;
    private array    $imageSizes;
    private string   $destinationPath;
    private array    $thumbnails;
    private bool     $optimized = false;
    private bool     $resized   = false;


    /**
     * ImageManipulator constructor.
     *
     * @param array       $imageSizes
     * @param string      $projectDir
     * @param File        $file
     * @param null|string $filename
     *
     * @throws \ImagickException
     */
    public function __construct(array $imageSizes, string $projectDir, File $file, ?string $filename)
    {
        $this->projectDir = $projectDir;
        $this->imagick    = new Imagick($file->getRealPath());
        $this->imageSizes = $imageSizes;

        $this->imagick->setImageFilename($filename ?? uniqid() . '.' . $file->getExtension());
        $this->width  = $this->imagick->getImageWidth();
        $this->height = $this->imagick->getImageHeight();
    }


    /**
     * @param int $maxHeight
     *
     * @return $this
     */
    public function setMaxHeight(?int $maxHeight)
    {
        $this->maxHeight = $maxHeight;

        return $this;
    }


    /**
     * @param int $maxWidth
     *
     * @return $this
     */
    public function setMaxWidth(?int $maxWidth)
    {
        $this->maxWidth = $maxWidth;

        return $this;
    }

    public function generateThumbnails()
    {
        $maxHeight        = $this->maxHeight;
        $maxWidth         = $this->maxWidth;
        $originalImagick  = $this->imagick;
        $this->thumbnails = [];

        foreach ($this->imageSizes as $label => $imageSize) {
            $this->maxWidth  = $imageSize['w'];
            $this->maxHeight = $imageSize['h'];
            $this->imagick   = $originalImagick->clone();
            $this->imagick->setImageFilename(
                '.'.pathinfo($originalImagick->getImageFilename(), PATHINFO_FILENAME).'-'.$label.'.'.pathinfo($originalImagick->getImageFilename(), PATHINFO_EXTENSION)
            );

            $this->resize($imageSize['crop'] ?? false);

            $this->thumbnails[] = $this->imagick;
        }

        $this->maxHeight = $maxHeight;
        $this->maxWidth  = $maxWidth;
        $this->imagick   = $originalImagick;

        return $this;
    }


    /**
     * @param bool $allowCrop
     *
     * @param bool $optimizeSize
     *
     * @return $this
     * @throws \Exception
     */
    public function resize($allowCrop = true, $optimizeSize = true)
    {
        $this->allowCrop    = $allowCrop;
        $this->optimizeSize = $optimizeSize;

        if (is_null($this->maxWidth)) {
            $this->setMaxWidth($this->width);
        }

        if (is_null($this->maxHeight)) {
            $this->setMaxHeight($this->height);
        }

        $this->defineNewDimensions();

        $this->optimizeSize();
        $this->processResize();

        return $this;
    }


    /**
     * @param $path
     *
     * @return $this
     * @throws \Exception
     */
    public function move()
    {
        if (!$this->optimized) {
            $this->optimizeSize();
        }

        $this->filepath = $this->destinationPath . DIRECTORY_SEPARATOR . $this->imagick->getImageFilename();

        if (!$this->imagick->writeImage($this->filepath)) {
            throw new \Exception("Unable to write image");
        }

        /** @var Imagick $thumbnail */
        foreach ($this->thumbnails as $thumbnail) {
            $filepath = $this->destinationPath . DIRECTORY_SEPARATOR . $thumbnail->getImageFilename();
            if (!$thumbnail->writeImage($filepath)) {
                throw new \Exception("Unable to write thumbnail");
            }
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function getResizedImageHeight()
    {
        return $this->newHeight;
    }


    /**
     * @return mixed
     */
    public function getResizedImageWidth()
    {
        return $this->newWidth;
    }


    /**
     * @return mixed
     */
    public function getResizedImagePath()
    {
        return $this->filepath;
    }


    /**
     *
     */
    private function defineNewDimensions()
    {
        $this->newHeight = $this->maxHeight;
        $this->newWidth  = $this->maxWidth;
        $x               = 0;
        $y               = 0;
        $ratioHaut       = $this->maxHeight / $this->height;
        $ratioLarg       = $this->maxWidth / $this->width;

        if ($this->width > $this->maxWidth && $this->height > $this->maxHeight) {
            if ($ratioHaut >= $ratioLarg) {
                $this->newWidth = $this->width * $ratioHaut;
                $x              = round(($this->maxWidth - $this->newWidth) / 2);
            } else {
                $this->newHeight = $this->height * $ratioLarg;
                $y               = round(($this->maxHeight - $this->newHeight) / 2);
            }
        } elseif ($this->width >= $this->maxWidth && $this->height <= $this->maxHeight) {
            if ($this->allowCrop) {
                $y = round(($this->maxHeight - $this->height) / 2);
            } else {
                $this->newHeight = $this->height * $ratioLarg;
            }
        } elseif ($this->width <= $this->maxWidth && $this->height >= $this->maxHeight) {
            if ($this->allowCrop) {
                $x = round(($this->maxWidth - $this->width) / 2);
            } else {
                $this->newWidth = $this->width * $ratioHaut;
            }
        } elseif ($this->width <= $this->maxWidth && $this->height <= $this->maxHeight) {
            $this->newWidth  = $this->width;
            $this->newHeight = $this->height;
            $x               = round(($this->width - $this->maxWidth) / 2);
            $y               = round(($this->height - $this->maxHeight) / 2);
        }

        $this->x = $x;
        $this->y = $y;
    }


    /**
     * @throws \Exception
     */
    private function processResize()
    {
        $this->resized = true;
        try {
            $this->imagick->adaptiveResizeImage($this->newWidth, $this->newHeight);
            $this->imagick->setImageOrientation($this->imagick->getImageOrientation());

            if ($this->allowCrop) {
                $this->imagick->cropImage($this->maxWidth, $this->maxHeight, abs($this->x), abs($this->y));
            }
        } catch (\Exception $e) {
            throw new \Exception("Unable to resize source image");
        }
    }

    public function optimizeSize()
    {
        $this->optimized = true;

        if ($this->optimizeSize) {
            switch ($this->imagick->getImageMimeType()) {
                case 'image/jpg':
                case 'image/jpeg':
                    $this->imagick->setCompression(Imagick::COMPRESSION_JPEG);
                    $this->imagick->setImageCompressionQuality(90);
                    break;

                case 'image/png':
                case 'image/gif':
                    if ($this->imagick->getImageAlphaChannel()) {
                        $this->imagick->setImageBackgroundColor(new ImagickPixel('transparent'));
                    }
                    $this->imagick->setOption('png:compression-level', 9);
                    break;
            }
        }
    }

    /**
     * @param string $path
     * @param bool   $createDirIfNotExist
     *
     * @return MagickManipulator
     * @throws \Exception
     */
    public function setDestinationPath(string $path, bool $createDirIfNotExist = false)
    {
        $this->destinationPath = $this->projectDir.$path;

        if (!is_dir($this->destinationPath)) {
            if (!$createDirIfNotExist) {
                throw new \Exception(sprintf("Directory %s does not exist", $path));
            }

            mkdir($this->destinationPath, 0777, true);
        }

        return $this;
    }
}
