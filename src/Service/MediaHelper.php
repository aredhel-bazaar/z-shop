<?php

namespace App\Service;

class MediaHelper
{
    const MEDIA_TYPE_DIRECTORY = 0;
    const MEDIA_TYPE_FILE      = 1;
    const MEDIA_TYPE_TEXT      = 2;
    const MEDIA_TYPE_IMAGE     = 3;
    const MEDIA_TYPE_AUDIO     = 4;
    const MEDIA_TYPE_VIDEO     = 5;
    const MEDIA_TYPE_ARCHIVE   = 6;
    const MEDIA_TYPE_PDF       = 7;
    const MEDIA_TYPE_EXE       = 8;

    const MEDIA_TYPES = [
        self::MEDIA_TYPE_DIRECTORY => 'directory',
        self::MEDIA_TYPE_FILE      => 'file',
        self::MEDIA_TYPE_TEXT      => 'text',
        self::MEDIA_TYPE_IMAGE     => 'image',
        self::MEDIA_TYPE_AUDIO     => 'audio',
        self::MEDIA_TYPE_VIDEO     => 'video',
        self::MEDIA_TYPE_ARCHIVE   => 'archive',
        self::MEDIA_TYPE_PDF       => 'pdf',
        self::MEDIA_TYPE_EXE       => 'executable',
    ];

    public function getTypeInfo(string $filename): array
    {
        return explode('/', mime_content_type($filename));
    }

    public function getMediaType(string $filename): string
    {
        $typeInfo = $this->getTypeInfo($filename);
        $fileExt  = pathinfo($filename, PATHINFO_EXTENSION);
        switch ($typeInfo[0]) {
            case 'inode':
                $mediaType = self::MEDIA_TYPE_DIRECTORY;
                break;

            case 'text':
                $mediaType = self::MEDIA_TYPE_TEXT;
                break;

            case 'image':
                $mediaType = self::MEDIA_TYPE_IMAGE;
                break;

            case 'audio':
                $mediaType = self::MEDIA_TYPE_AUDIO;
                break;

            case 'video':
            case 'x-ms-asf':
                if ($fileExt === 'wma') {
                    $mediaType = self::MEDIA_TYPE_AUDIO;
                } else {
                    $mediaType = self::MEDIA_TYPE_VIDEO;
                }
                break;

            case 'application':
                switch ($typeInfo[1]) {
                    case 'x-rar':
                    case 'zip':
                    case 'x-iso9660-image':
                        $mediaType = self::MEDIA_TYPE_ARCHIVE;
                        break;

                    case 'pdf':
                        $mediaType = self::MEDIA_TYPE_PDF;
                        break;

                    case 'x-bittorrent':
                        $mediaType = self::MEDIA_TYPE_TEXT;
                        break;

                    case 'mp3':
                        $mediaType = self::MEDIA_TYPE_AUDIO;
                        break;

                    case 'x-dosexec':
                    case 'x-msi':
                        $mediaType = self::MEDIA_TYPE_EXE;
                        break;

                    case 'vnd.oasis.opendocument.text':
                        $mediaType = self::MEDIA_TYPE_FILE;
                        break;

                    default:
                        $mediaType = self::MEDIA_TYPE_FILE;
                }
                break;

            default:
                $mediaType = self::MEDIA_TYPE_FILE;
        }

        return self::MEDIA_TYPES[$mediaType];
    }

    public function getMetadata(string $filename): array
    {
        return [
            'filename'  => pathinfo($filename, PATHINFO_BASENAME),
            'filesize'  => filesize($filename),
            'filectime' => filectime($filename),
            'fileatime' => fileatime($filename),
            'filemtime' => filemtime($filename),
        ];
    }
}
