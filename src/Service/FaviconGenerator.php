<?php

namespace App\Service;

use App\Entity\Media;
use App\EventSubscriber\ShopLoader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class FaviconGenerator
{
    /**
     * @var ShopLoader
     */
    private ShopLoader $shopLoader;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ShopLoader $shopLoader, EntityManagerInterface $entityManager)
    {
        $this->shopLoader = $shopLoader;
        $this->entityManager = $entityManager;
    }

    public function generate(string $filepath)
    {
        if (!file_exists($filepath)) {
            throw new FileNotFoundException($filepath);
        }

        $imagick = new \Imagick();
        $imagick->setFormat('ico');
        foreach ([16, 32, 48] as $size) {
            $thumb = new \Imagick($filepath);
            $thumb->setFormat('ico');
            $thumb->adaptiveResizeImage($size, $size);

            $imagick->addImage($thumb);
        }

        $imagick->writeImages($this->shopLoader->getPublicPath().'/favicon.ico', true);
        $media = new Media();
        $media->setFilename('favicon.ico')
            ->setName('Favicon')
            ->setPath('/')
            ->setShop($this->shopLoader->getLoadedShop())
            ->setStatus(Media::STATUS_NORMAL)
        ;
        $this->entityManager->persist($media);
        $this->entityManager->flush();
    }
}