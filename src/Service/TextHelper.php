<?php

namespace App\Service;

use Symfony\Component\HttpClient\CurlHttpClient;

class TextHelper
{
    public static function getLoremIpsum(int $amount = 5, string $what = 'paras')
    {
        $curl = new CurlHttpClient();
        try {
            $response = $curl->request('GET', "https://fr.lipsum.com/feed/json?amount=$amount&what=$what&start=yes");
            $payload  = $response->toArray();
        } catch (\Exception $e) {
            //-- TODO
            return;
        }

        return $payload['feed']['lipsum'];
    }

    public static function humanFilesize($value)
    {
        $units = ['o', 'Ko', 'Mo', 'Go', 'To'];
        $bytes = max($value, 0);
        $pow   = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow   = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);

        return round($bytes, 2) . ' ' . $units[$pow];
    }

    public static function intToBase36(string $text)
    {
        return base_convert($text, 10, 36);
    }
}