<?php

namespace App\Service;

use Cloudflare\API\Adapter\Guzzle;
use Cloudflare\API\Auth\APIKey;
use Cloudflare\API\Endpoints\Zones;
use Psr\Cache\CacheItemPoolInterface;

class Cloudflare
{
    const SEVEN_DAYS = 10080;
    const THREE_DAYS = 3360;
    const ONE_DAY = 1440;

    const CONFIGS = [
        self::SEVEN_DAYS,
        self::THREE_DAYS,
        self::ONE_DAY
    ];

    /**
     * @var string
     */
    private $cloudflareUser;
    /**
     * @var string
     */
    private $cloudflareApiKey;
    /**
     * @var string
     */
    private $cloudflareZoneId;
    /**
     * @var CacheItemPoolInterface
     */
    private $cacheItemPool;

    public function __construct(string $cloudflareUser, string $cloudflareApiKey, string $cloudflareZoneId, CacheItemPoolInterface $cacheItemPool)
    {
        $this->cloudflareUser   = $cloudflareUser;
        $this->cloudflareApiKey = $cloudflareApiKey;
        $this->cloudflareZoneId = $cloudflareZoneId;
        $this->cacheItemPool    = $cacheItemPool;
    }

    /**
     * @param $since
     * @param $until
     *
     * @return mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getDashboard($since, $until)
    {
        $cachedDashboard = $this->cacheItemPool->getItem("cloudflare.dashboard.".abs($since).".$until");
        if (!$cachedDashboard->isHit()) {
            $this->buildCache();
            return $this->getDashboard($since, $until);
        }

        return $cachedDashboard->get();
    }

    /**
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function buildCache()
    {
        foreach (self::CONFIGS as $config) {
            $cachedDashboard = $this->cacheItemPool->getItem("cloudflare.dashboard.{$config}.0");

            $key     = new APIKey($this->cloudflareUser, $this->cloudflareApiKey);
            $adapter = new Guzzle($key);
            $zones   = new Zones($adapter);

            try {
                $analytics = $zones->getAnalyticsDashboard($this->cloudflareZoneId, -$config, 0);
            } catch (\Exception $e) {
                $analytics = [];
            }

            $cachedDashboard->set($analytics);
            $cachedDashboard->expiresAfter($config * 180);
            $this->cacheItemPool->save($cachedDashboard);
        }
    }
}
