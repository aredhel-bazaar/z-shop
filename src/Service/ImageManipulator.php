<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

class ImageManipulator
{
    /**
     * @var string
     */
    private $projectDir;
    /**
     * @var array
     */
    private $imageSizes;


    /**
     * ImageManipulator constructor.
     *
     * @param array  $imageSizes
     * @param string $projectDir
     */
    public function __construct(array $imageSizes, string $projectDir)
    {
        $this->projectDir = $projectDir;
        $this->imageSizes = $imageSizes;
    }


    /**
     * @param File        $file
     * @param string|null $filename
     *
     * @return MagickManipulator
     * @throws \ImagickException
     */
    public function init(File $file, string $filename = null)
    {
        return new MagickManipulator($this->imageSizes, $this->projectDir, $file, $filename);
    }
}
