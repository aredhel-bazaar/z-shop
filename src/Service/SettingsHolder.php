<?php

namespace App\Service;

use App\Entity\Setting;
use Doctrine\ORM\PersistentCollection;

class SettingsHolder implements \ArrayAccess, \Iterator
{
    /**
     * @var Setting[]
     */
    private $settings;
    /**
     * @var array
     */
    private $keys;

    /**
     * @param Setting[] $settings
     */
    public function load(PersistentCollection $settings)
    {
        $this->settings = $settings;
        $this->keys     = [];
        foreach ($this->settings as $index => $setting) {
            $this->keys[$setting->getId()] = $index;
        }
    }

    public function __get($key)
    {
        return $this->settings[$this->keys[$key]];
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->keys);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        return $this->settings[$this->keys[$offset]]->getValue();
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value): void
    {
        die("offsetSet ".$offset);
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset): void
    {
        die("offsetUnset ".$offset);
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        die("current");
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        die("next");
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        die("key");
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        die("valid");
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        die("rewind");
    }
}
