<?php

namespace App\Service;

class FilesystemHelper
{
    /**
     * @param string $directory
     * @param bool   $recursive
     *
     * @return int
     */
    public static function getDirectorySize(string $directory, bool $recursive = true)
    {
        $size = 0;
        foreach (glob(rtrim($directory, '/').'/*', GLOB_NOSORT) as $each) {
            if (substr($each, 0, 1) === '.') { continue; }

            $size += is_file($each) ? (int) filesize($each) : (true === $recursive ? self::getDirectorySize($each) : 0);
        }
        return $size;
    }
}
