<?php

namespace App\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class FrontController extends Controller
{
    #[Route("/", name:"homepage")]
    public function homepage()
    {
        return $this->render("homepage.html.twig");
    }

    public function customPage(Environment $environment, string $template)
    {
        if ($environment->getLoader()->exists($template)) {
            return $this->render($template);
        }

        throw new NotFoundHttpException();
    }
}
