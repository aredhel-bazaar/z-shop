<?php

namespace App\Controller;

use App\EventSubscriber\ShopLoader;
use App\Service\UserAgentAPI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

abstract class Controller extends AbstractController
{
    /**
     * @var ShopLoader
     */
    protected $shopLoader;
    /**
     * @var \App\Entity\UserAgent
     */
    private $userAgent;

    public function __construct(ShopLoader $shopLoader, UserAgentAPI $userAgentAPI)
    {
        $this->shopLoader = $shopLoader;
        $this->userAgent  = $userAgentAPI->getUserAgent();
    }

    /**
     * Renders a view.
     *
     * @final
     *
     * @param string        $view
     * @param array         $parameters
     * @param Response|null $response
     *
     * @return Response
     */
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $this->addDefaultParameters($parameters);

        return parent::render($view, $parameters, $response);
    }

    /**
     * Returns a rendered view.
     *
     * @final
     *
     * @param string $view
     * @param array  $parameters
     *
     * @return string
     */
    protected function renderView(string $view, array $parameters = []): string
    {
        $this->addDefaultParameters($parameters);

        return parent::renderView($view, $parameters);
    }

    /**
     * @param array $parameters
     */
    private function addDefaultParameters(array &$parameters)
    {
        if (!isset($parameters['ua'])) {
            $parameters['ua'] = $this->userAgent;
        }
    }
}
