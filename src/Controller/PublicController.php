<?php

namespace App\Controller;

use App\Entity\Media;
use App\Repository\MediaRepository;
use App\Service\ImageManipulator;
use App\Service\Manipulator;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PublicController
 * @package App\Controller
 */
class PublicController extends Controller
{
    #[Route("/pub{path}", name:"app.public.get", requirements:["path" => ".*"])]
    public function getFile(string $path, MediaRepository $mediaRepository, ImageManipulator $manipulator)
    {
        $pathinfo = pathinfo($path);
        $fullPath = $this->shopLoader->getPublicPath().DIRECTORY_SEPARATOR.$path;

        preg_match("/^\..*-([a-z]{1,2})$/", $pathinfo['filename'], $sizeMatches);
        if (!empty($sizeMatches)) {
            $originalFilename = preg_replace("/-[a-z]{1,2}$/", "", substr($pathinfo['filename'], 1)).'.'.$pathinfo['extension'];
        } else {
            $originalFilename = $pathinfo['basename'];
        }
        /** @var Media|null $media */
        $media = $mediaRepository->findOneBy(['path' => $pathinfo['dirname'], 'filename' => $originalFilename]);

        if (null !== $media && !empty($sizeMatches) && $media->supportsThumbnail() && false === file_exists($fullPath)) {
            $manipulator->init(new File($this->shopLoader->getPublicPath(). $media->getFullPath()), $media->getFilename())
                ->setDestinationPath($this->shopLoader->getPublicPath(true). $media->getPath(), true)
                ->generateThumbnails()
                ->move()
            ;
        }

        if ($media && file_exists($fullPath)) {
            return new BinaryFileResponse($fullPath);
        }

        return new Response(null, Response::HTTP_NOT_FOUND);
    }

    #[Route("/stream/{path}", name:"app.public.stream", requirements:["path" => ".*"])]
    public function streamFile(string $path)
    {
        $fullPath = $this->shopLoader->getPublicPath().DIRECTORY_SEPARATOR.$path;

        if (file_exists($fullPath)) {
            return new BinaryFileResponse($fullPath);
        }

        return new Response(null, Response::HTTP_NOT_FOUND);
    }
}