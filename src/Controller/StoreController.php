<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StoreController
 * @package App\Controller
 *
 * @Route("/store", name="store")
 */
class StoreController extends AbstractController
{
    /**
     * @Route("/", name="store.home")
     */
    public function homepage()
    {
        return $this->render("store/home.html.twig");
    }
}