<?php

namespace App\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;

class TemplateController extends Controller
{
    public function template(Environment $environment, string $template)
    {
        //if ($environment->getLoader()->exists($template)) {
            return $this->render($template);
        //}

        throw new NotFoundHttpException();
    }
}
