<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package App\Controller\Admin
 *
 * @Route("/admin")
 */
class SecurityController extends AdminController
{
    /**
     * @Route("/login", name="app.admin.security.login")
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        return $this->render('admin/security/login.html.twig', [
            'error'        => $authenticationUtils->getLastAuthenticationError(),
            'lastUsername' => $authenticationUtils->getLastUsername()
        ]);
    }

    /**
     * @Route("/logout", name="app.admin.security.logout")
     */
    public function logout()
    {
        throw new AccessDeniedHttpException();
    }
}
