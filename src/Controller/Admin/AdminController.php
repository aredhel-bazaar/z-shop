<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller\Admin
 *
 * @Route("/admin")
*/
class AdminController extends Controller
{
}