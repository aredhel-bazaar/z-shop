<?php

namespace App\Controller\Admin;

use App\Form\OrderFilterType;
use App\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller\Admin
 *
 * @Route("/admin/order")
 */
class OrderController extends Controller
{
    /**
     * @Route("/list", name="app.admin.order.list")
     *
     * @param Request         $request
     * @param OrderRepository $orderRepository
     *
     * @return Response
     */
    public function list(Request $request, OrderRepository $orderRepository)
    {
        $currentPage = max($request->query->getInt('page', 1), 1);

        $productFilterForm = $this->createForm(OrderFilterType::class);

        $productFilterForm->handleRequest($request);

        $formData  = $productFilterForm->getData();
        $nbPerPage = $formData['nb_per_page'] ?? 25;
        $orders    = $orderRepository->searchForOrders($formData, ($currentPage-1) * $nbPerPage, $nbPerPage);
        $total     = $orderRepository->countAll($formData);

        return $this->render('admin/order/list.html.twig', [
            'total'        => $total,
            'orders'       => $orders,
            'nbPages'      => ceil($total / $nbPerPage),
            'currentPage'  => $currentPage,
            'filterForm'   => $productFilterForm->createView()
        ]);
    }
}
