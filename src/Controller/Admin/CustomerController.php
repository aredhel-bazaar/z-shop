<?php

namespace App\Controller\Admin;

use App\Form\CustomerFilterType;
use App\Repository\CustomerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller\Admin
 *
 * @Route("/admin/customer")
*/
class CustomerController extends Controller
{
    /**
     * @Route("/list", name="app.admin.customer.list")
     *
     * @param Request            $request
     *
     * @param CustomerRepository $customerRepository
     *
     * @return Response
     */
    public function list(Request $request, CustomerRepository $customerRepository)
    {
        $currentPage = max($request->query->getInt('page', 1), 1);

        $productFilterForm = $this->createForm(CustomerFilterType::class);

        $productFilterForm->handleRequest($request);

        $formData  = $productFilterForm->getData();
        $nbPerPage = $formData['nb_per_page'] ?? 25;
        $customers = $customerRepository->searchForCustomers($formData, ($currentPage - 1) * $nbPerPage, $nbPerPage);
        $total     = $customerRepository->countAll($formData);

        return $this->render('admin/customer/list.html.twig', [
            'total'       => $total,
            'customers'   => $customers,
            'nbPages'     => ceil($total / $nbPerPage),
            'currentPage' => $currentPage,
            'filterForm'  => $productFilterForm->createView()
        ]);
    }
}