<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\EventSubscriber\ShopLoader;
use App\Form\ProductFilterType;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package App\Controller
 *
 * @Route("/admin/product")
 */
class ProductController extends Controller
{
    /**
     * @Route("s", name="app.admin.product.list")
     *
     * @param Request           $request
     * @param ProductRepository $productRepository
     *
     * @return Response
     */
    public function products(Request $request, ProductRepository $productRepository)
    {
        $currentPage = max($request->query->getInt('page', 1), 1);

        $productFilterForm = $this->createForm(ProductFilterType::class);

        $productFilterForm->handleRequest($request);

        $formData  = $productFilterForm->getData();
        $nbPerPage = $formData['nb_per_page'] ?? 25;
        $products  = $productRepository->searchForProducts($formData, ($currentPage-1) * $nbPerPage, $nbPerPage);
        $total     = $productRepository->countAll($formData);

        return $this->render('admin/product/list.html.twig', [
            'total'        => $total,
            'products'     => $products,
            'nbPages'      => ceil($total / $nbPerPage),
            'currentPage'  => $currentPage,
            'filterForm'   => $productFilterForm->createView()
        ]);
    }

    /**
     * @Route("/create", name="app.admin.product.create")
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(ProductType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var Product $product */
                $product = $form->getData();

                $product->setShop($this->shopLoader->getLoadedShop());

                $entityManager->persist($product);
                $entityManager->flush();

                return $this->redirectToRoute('app.admin.product.update', ['product' => $product->getId()]);
            }
        }

        return $this->render('admin/product/create.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/{product}/update", name="app.admin.product.update")
     *
     * @param Product                $product
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function update(Product $product, Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $entityManager->flush();
            }
        }

        return $this->render('admin/product/update.html.twig', [
            'form'    => $form->createView(),
            'product' => $product
        ]);
    }
}
