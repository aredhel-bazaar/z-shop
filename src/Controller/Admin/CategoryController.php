<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Form\CategoryFilterType;
use App\Form\ProductType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller\Admin
 *
 */
#[Route("/admin/category")]
class CategoryController extends Controller
{
    #[Route("/list", name:"app.admin.category.list")]
    public function list(Request $request, CategoryRepository $categoryRepository)
    {
        $currentPage = max($request->query->getInt('page', 1), 1);

        $productFilterForm = $this->createForm(CategoryFilterType::class);

        $productFilterForm->handleRequest($request);

        $formData   = $productFilterForm->getData();
        $nbPerPage  = $formData['nb_per_page'] ?? 25;
        $categories = $categoryRepository->searchForCategories($formData, ($currentPage - 1) * $nbPerPage, $nbPerPage);
        $total      = $categoryRepository->countAll($formData);

        return $this->render('admin/category/list.html.twig', [
            'total'       => $total,
            'categories'  => $categories,
            'nbPages'     => ceil($total / $nbPerPage),
            'currentPage' => $currentPage,
            'filterForm'  => $productFilterForm->createView()
        ]);
    }

    #[Route("/create", name:"app.admin.category.create")]
    public function create(EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(ProductType::class);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $product = $form->getData();

                $entityManager->persist($product);
                $entityManager->flush();

                return $this->redirectToRoute('app.admin.product.update');
            }
        }

        return $this->render('admin/category/create.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/{product}/update", name="app.admin.category.update")
     *
     * @param Product                $product
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function update(Product $product, Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $entityManager->flush();
            }
        }

        return $this->render('admin/category/update.html.twig', [
            'form'    => $form->createView(),
            'product' => $product
        ]);
    }
}