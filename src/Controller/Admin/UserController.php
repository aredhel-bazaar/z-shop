<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\ShopFilterType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class ShopController
 * @package App\Admin\Controller
 *
 * @Route("/admin/user")
 */
class UserController extends Controller
{
    /**
     * @Route("s", name="app.admin.user.list")
     *
     * @param Request        $request
     * @param UserRepository $userRepository
     *
     * @return Response
     */
    public function users(Request $request, UserRepository $userRepository)
    {
        $currentPage = max($request->query->getInt('page', 1), 1);

        $productFilterForm = $this->createForm(ShopFilterType::class);

        $productFilterForm->handleRequest($request);

        $formData  = $productFilterForm->getData();
        $nbPerPage = $formData['nb_per_page'] ?? 25;
        $users     = $userRepository->searchForUsers($formData, ($currentPage - 1) * $nbPerPage, $nbPerPage);
        $total     = $userRepository->countAll($formData);

        return $this->render('admin/user/list.html.twig', [
            'total'       => $total,
            'users'       => $users,
            'nbPages'     => ceil($total / $nbPerPage),
            'currentPage' => $currentPage,
            'filterForm'  => $productFilterForm->createView()
        ]);
    }

    /**
     * @Route("/create", name="app.admin.user.create")
     *
     * @param Request                      $request
     * @param EntityManagerInterface       $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var User $user */
                $user = $form->getData();
                $user->setSalt(bin2hex(openssl_random_pseudo_bytes(16)))
                    ->setPassword($passwordEncoder->encodePassword($user, $user->getPlainPassword()))
                ;


                $entityManager->persist($user);
                $entityManager->flush();

                return $this->redirectToRoute('app.admin.user.create');
            }
        }

        return $this->render('admin/user/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/update/{user}", name="app.admin.user.update")
     *
     * @param User                         $user
     * @param Request                      $request
     * @param EntityManagerInterface       $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return Response
     */
    public function update(User $user, Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($user->getPlainPassword()) {
                    $user->setSalt(bin2hex(openssl_random_pseudo_bytes(16)))
                        ->setPassword($passwordEncoder->encodePassword($user, $user->getPlainPassword()))
                    ;
                }

                $entityManager->flush();

                return $this->redirectToRoute('app.admin.user.update', ['user' => $user->getId()]);
            }
        }

        return $this->render('admin/user/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function delete()
    {

    }
}
