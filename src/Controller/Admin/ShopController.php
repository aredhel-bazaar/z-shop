<?php

namespace App\Controller\Admin;

use App\Form\ShopFilterType;
use App\Form\ShopSettingsType;
use App\Form\ShopType;
use App\Repository\ShopRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShopController
 * @package App\Admin\Controller
 *
 * @Route("/admin/shop")
 */
class ShopController extends Controller
{
    /**
     * @Route("s", name="app.admin.shop.list")
     *
     * @param Request        $request
     * @param ShopRepository $shopRepository
     *
     * @return Response
     */
    public function shops(Request $request, ShopRepository $shopRepository)
    {
        $currentPage = max($request->query->getInt('page', 1), 1);

        $productFilterForm = $this->createForm(ShopFilterType::class);

        $productFilterForm->handleRequest($request);

        $formData  = $productFilterForm->getData();
        $nbPerPage = $formData['nb_per_page'] ?? 25;
        $shops     = $shopRepository->searchForShops($formData, ($currentPage - 1) * $nbPerPage, $nbPerPage);
        $total     = $shopRepository->countAll($formData);

        return $this->render('admin/shop/list.html.twig', [
            'total'       => $total,
            'shops'       => $shops,
            'nbPages'     => ceil($total / $nbPerPage),
            'currentPage' => $currentPage,
            'filterForm'  => $productFilterForm->createView()
        ]);
    }

    /**
     * @Route("/create", name="app.admin.shop.create")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(ShopType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $shop = $form->getData();

                $entityManager->persist($shop);
                $entityManager->flush();
            }
        }

        return $this->render('admin/shop/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function delete()
    {

    }

    /**
     * @Route("/settings", name="app.admin.shop.settings")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function settings(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(ShopSettingsType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $config = $form->getData();

                $this->shopLoader->saveConfig($config);

                return $this->redirectToRoute('app.admin.shop.settings');
            }
        }

        return $this->render('admin/shop/settings.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
