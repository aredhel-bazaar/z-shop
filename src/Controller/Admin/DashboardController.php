<?php

namespace App\Controller\Admin;

use App\Controller\Controller;
use App\Entity\Task;
use App\Service\DashboardData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 * @package App\Controller\Admin
 *
 * @Route("/admin")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/", name="app.admin.dashboard")
     *
     * @param DashboardData $dashboardData
     *
     * @return Response
     */
    public function index(DashboardData $dashboardData)
    {
        return $this->render('admin/dashboard.html.twig',
            [
                'dashboard' => $dashboardData->getData(DashboardData::SEVEN_DAYS)
            ]
        );
    }
}
