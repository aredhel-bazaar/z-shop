<?php

namespace App\Controller\Admin;

use App\Entity\Media;
use App\Entity\Task;
use App\Repository\MediaRepository;
use App\Service\FaviconGenerator;
use App\Service\ImageManipulator;
use App\Service\MediaHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use ImagickException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class MediaController
 * @package App\Controller\Admin
 *
 * @Route("/admin/media")
 */
class MediaController extends Controller
{
    /**
     * @Route("/list/{path}", name="app.admin.media.list", requirements={"path": ".*"})
     *
     * @param MediaRepository $mediaRepository
     * @param string          $path
     *
     * @return Response
     */
    public function list(MediaRepository $mediaRepository, string $path = '')
    {
        if (strlen($path) === 0 || substr($path, 0, 1) !== '/') {
            $path = '/'.$path;
        }

        if (strlen($path) > 1 && substr($path, -1) === '/') {
            $path = substr($path, 0, -1);
        }

        $basePath = $this->shopLoader->getPublicPath();
        if (!is_dir($basePath)) {
            mkdir($basePath);
        }

        if (!is_dir($basePath.DIRECTORY_SEPARATOR.$path)) {
            return $this->redirectToRoute('app.admin.media.list');
        }

        $directories = array_filter(array_map(function ($dir) use ($basePath, $path) { return str_replace($basePath.DIRECTORY_SEPARATOR.$path, '', $dir); }, glob($basePath.DIRECTORY_SEPARATOR.$path.'*', GLOB_ONLYDIR)));
        foreach ($directories as $index => $directory) {
            $directories[$index] = [
                'name'                 => $directory,
                'creationDatetime'     => filectime($basePath.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$directory),
                'modificationDatetime' => filemtime($basePath.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$directory)
            ];
        }

        return $this->render('admin/media/list.html.twig', [
            'pathList'    => array_filter(explode('/', $path)),
            'path'        => substr($path, 1),
            'directories' => $directories,
            'medias'      => $mediaRepository->findBy(['path' => $path])
        ]);
    }

    /**
     * @Route("/upload", name="app.admin.media.upload")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param MediaRepository        $mediaRepository
     * @param UrlGeneratorInterface  $router
     * @param FaviconGenerator       $faviconGenerator
     *
     * @return Response
     */
    public function uploadMedia(Request $request, EntityManagerInterface $entityManager, MediaRepository $mediaRepository, UrlGeneratorInterface $router, FaviconGenerator $faviconGenerator)
    {
        $media = null;

        /** @var UploadedFile $file */
        $file           = $request->files->get('file');
        $id             = $request->request->get('id');
        $subPath        = preg_replace("/\/{2,}/", '/', '/'.$request->request->get('path', '/'));
        $allowOverwrite = $request->request->getBoolean('allow_overwrite', ($id ? true : false));

        if (!($file instanceof UploadedFile)) {
            return new Response("Error while uploading file", Response::HTTP_BAD_REQUEST);
        }

        if ($file->getSize() + $this->shopLoader->getLoadedShop()->getDiskUsage() > $this->shopLoader->getConfigItem('quota')) {
            return new Response("Quota would be reached", Response::HTTP_BAD_REQUEST);
        }

        if ($id) {
            /** @var Media $media */
            $media    = $mediaRepository->find($id);
            $path     = $this->shopLoader->getPublicPath(true) . $subPath;
            $name     = $media->getName();
            $basename = $media->getFilename();
        } else {
            $path     = $this->shopLoader->getPublicPath(true) . $subPath;
            $name     = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $basename = $file->getClientOriginalName();

            /** @var Media $media */
            $media = $mediaRepository->findOneBy(['path' => $subPath, 'filename' => $basename]);
        }

        if (false === $allowOverwrite && null !== $media && Media::STATUS_DELETED !== $media->getStatus()) {
            return new Response("Media already exist", Response::HTTP_BAD_REQUEST);
        }

        $i = 1;
        while (file_exists($path.DIRECTORY_SEPARATOR.$basename) && !$allowOverwrite) {
            $basename = $name.'-'.$i.'.'.pathinfo($name, PATHINFO_EXTENSION);
            $i++;
        }

        if (!is_null($media) && $allowOverwrite || $id) {
            if ($media->getStatus() === Media::STATUS_DELETED) {
                $media
                    ->setStatus(Media::STATUS_NORMAL)
                    ->setCreationDatetime(new DateTime())
                    ->setUser($this->getUser())
                ;
            }
            $media->setModificationDatetime(new DateTime());
            $entityManager->getUnitOfWork()->scheduleForUpdate($media);
        } else {
            $media = new Media();
            $media->setPath($subPath)
                  ->setFilename($basename)
                  ->setName($name)
                  ->setShop($this->shopLoader->getLoadedShop())
                  ->setUser($this->getUser())
            ;

            $entityManager->persist($media);
        }

        $this->shopLoader->getLoadedShop()->setDiskUsage($this->shopLoader->getLoadedShop()->getDiskUsage()+$file->getSize());
        $entityManager->flush();

        $file->move($this->shopLoader->getPublicPath() . $media->getPath(), $basename);

        return new JsonResponse([
            'media' => $media,
            'size'  => filesize($this->shopLoader->getPublicPath() . $media->getFullPath()),
            'path'  => $router->generate('app.public.get', ['path' => $media->getFullPath()]),
            'url'   => $router->generate('app.admin.media.view', ['media' => $media->getId()]),
            'thumb' => ($media->supportsThumbnail() ? $router->generate('app.public.get', ['path' => $media->getThumbnail('sq')]) : null)
        ]);
    }

    /**
     * @Route("/view/{media}", name="app.admin.media.view", requirements={"media": ".*"})
     *
     *
     * @param Media       $media
     * @param MediaHelper $mediaHelper
     *
     * @return Response
     */
    public function view(Media $media, MediaHelper $mediaHelper)
    {
        $mediaType = $mediaHelper->getMediaType($this->shopLoader->getPublicPath().$media->getFullPath());

        return $this->render('admin/media/type/'.$mediaType.'.html.twig', [
            'media' => $media,
            'metadata' => $mediaHelper->getMetadata($this->shopLoader->getPublicPath().$media->getFullPath())
        ]);
    }

    /**
     * @Route("/delete/{media}", name="app.admin.media.delete", requirements={"media": ".*"}, methods={"DELETE"})
     *
     *
     * @param MediaRepository $mediaRepository
     * @param Filesystem $filesystem
     * @param EntityManagerInterface $entityManager
     * @param Media $media
     *
     * @return Response
     */
    public function delete(MediaRepository $mediaRepository, Filesystem $filesystem, EntityManagerInterface $entityManager, Media $media)
    {
        $this->mediaAction($mediaRepository, $filesystem, $entityManager, 'delete', [$media->getId()]);

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @Route("/rebuild-thumbnails/{media}", name="app.admin.media.rebuild-thumbnails", requirements={"media": ".*"})
     *
     * @param Request          $request
     * @param MediaRepository  $mediaRepository
     * @param ImageManipulator $manipulator
     * @param Media|null       $media
     *
     * @return Response
     * @throws ImagickException
     *
     * @deprecated
     */
    public function rebuildThumbnails(Request $request, MediaRepository $mediaRepository, ImageManipulator $manipulator, Media $media = null)
    {
        if (is_null($media)) {
            $medias = $mediaRepository->findAll();
        } else {
            $medias = [$media];
        }

        foreach ($medias as $media) {
            if (!$media->supportsThumbnail()) {
                continue;
            }

            $manipulator->init(new File($this->shopLoader->getPublicPath(). $media->getFullPath()), $media->getFilename())
                ->setDestinationPath($this->shopLoader->getPublicPath(true). $media->getPath(), true)
                ->generateThumbnails()
                ->move()
            ;
        }

        if ($request->server->get('HTTP_REFERER')) {
            return new RedirectResponse($request->server->get('HTTP_REFERER'));
        } else {
            return $this->redirectToRoute('app.admin.media.list');
        }
    }

    /**
     * @Route("/folder/create", name="app.admin.media.create-folder", methods={"POST"})
     *
     * @param Request    $request
     * @param Filesystem $filesystem
     *
     * @return RedirectResponse
     */
    public function createFolder(Request $request, Filesystem $filesystem)
    {
        $basePath = $request->request->get('basepath');
        $folder   = $request->request->get('folder');
        $realpath = realpath($this->shopLoader->getPublicPath().DIRECTORY_SEPARATOR.$basePath);

        if (strpos($realpath, $this->shopLoader->getPublicPath()) !== 0) {
            throw new AccessDeniedException($this->shopLoader->getPublicPath().$basePath);
        }

        $filesystem->mkdir($realpath.DIRECTORY_SEPARATOR.$folder);

        $path = preg_replace("/\/{2,}/", '/', $basePath.DIRECTORY_SEPARATOR.$folder);
        if (substr($path, 0, 1) === '/') {
            $path = substr($path, 1);
        }

        return $this->redirectToRoute('app.admin.media.list', [
            'path' => $path
        ]);
    }

    /**
     * @Route("/folder/delete/{folder}", name="app.admin.media.delete-folder", methods={"DELETE"}, requirements={"folder": ".*"})
     *
     * @param string                 $folder
     * @param Filesystem             $filesystem
     * @param EntityManagerInterface $entityManager
     * @param MediaRepository        $mediaRepository
     *
     * @param Request                $request
     *
     * @return Response
     */
    public function deleteFolder(string $folder, Filesystem $filesystem, EntityManagerInterface $entityManager, MediaRepository $mediaRepository, Request $request)
    {
        $path = realpath($this->shopLoader->getPublicPath().DIRECTORY_SEPARATOR.$folder);
        if (strpos($path, $this->shopLoader->getPublicPath()) !== 0) {
            //-- Security breach !
            die();
        }

        $mediaList = $mediaRepository->findByPathStart(DIRECTORY_SEPARATOR.$folder);

        if (!empty($mediaList)) {
            $this->mediaAction($mediaRepository, $filesystem, $entityManager, 'delete', array_map(function($media) { return $media->getId(); }, $mediaList));
        }

        $filesystem->remove($path);

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @Route("/batch", name="app.admin.media.batch", methods={"POST"})
     *
     * @param Filesystem             $filesystem
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param MediaRepository        $mediaRepository
     *
     * @return JsonResponse|Response
     */
    public function batch(Filesystem $filesystem, Request $request, EntityManagerInterface $entityManager, MediaRepository $mediaRepository)
    {
        $action   = $request->request->get('action');
        $ids      = explode(',', $request->request->get('ids'));
        $basePath = $request->request->get('basepath', null);

        if (is_null($basePath) || empty($action)) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        if (empty($ids)) {
            return new Response(null, Response::HTTP_OK);
        }

        $this->mediaAction($mediaRepository, $filesystem, $entityManager, $action, $ids);

        return new JsonResponse([
            'ids' => $ids
        ], Response::HTTP_OK);
    }

    /**
     * @param MediaRepository        $mediaRepository
     * @param Filesystem             $filesystem
     * @param EntityManagerInterface $entityManager
     * @param string                 $action
     * @param array                  $ids
     */
    private function mediaAction(MediaRepository $mediaRepository, Filesystem $filesystem, EntityManagerInterface $entityManager, string $action, array $ids)
    {
        switch ($action) {
            case 'delete':
                /** @var Media[] $mediaList */
                $mediaList = $mediaRepository->findBy(['id' => $ids]);
                if (empty($mediaList)) {
                    return;
                }

                $command = $entityManager->getRepository(Task::class)->findOneBy(['command' => 'app:media:clean']);
                if (null === $command) {
                    $task = new Task();
                    $task->setCommand('app:media:clean')
                        ->setPayload([])
                        ->setRecurrenceCount(1)
                        ->setRecurrenceInterval(new \DateInterval('PT1M'))
                        ->setExecutionDatetime(DateTime::createFromFormat('U', time()+60))
                    ;
                    $entityManager->persist($task);
                }

                foreach ($mediaList as $media) {
                    $media->setStatus(Media::STATUS_DELETED);
                }

                $entityManager->flush();
                break;
        }

        return;
    }
}
