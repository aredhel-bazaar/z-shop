<?php

namespace App\Controller\Admin;

use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TaskController
 * @package App\Controller\Admin
 *
 * @Route("/admin/task")
 */
class TaskController extends Controller
{
    /**
     * @Route("/list", name="app.admin.task.list")
     *
     * @param Request         $request
     * @param TaskRepository  $taskRepository
     * @param KernelInterface $kernel
     *
     * @return Response
     */
    public function list(Request $request, TaskRepository $taskRepository, KernelInterface $kernel)
    {
        $currentPage = max($request->query->getInt('page', 1), 1);

        $nbPerPage = 25;
        $tasks = $taskRepository->searchForTasks([], ($currentPage - 1) * $nbPerPage, $nbPerPage);
        $total = $taskRepository->countAll([]);

        $application = new Application($kernel);
        foreach ($tasks as $task) {
            $taskCommand = $application->find($task->getCommand());
            $task->setCommandInfos([
                'title'       => $taskCommand->getName(),
                'description' => $taskCommand->getDescription()
            ]);
        }

        return $this->render('admin/task/list.html.twig', [
            'total'       => $total,
            'tasks'       => $tasks,
            'nbPages'     => ceil($total / $nbPerPage),
            'currentPage' => $currentPage,
        ]);

    }
}