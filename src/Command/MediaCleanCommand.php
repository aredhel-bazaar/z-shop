<?php

namespace App\Command;

use App\Entity\Media;
use App\Entity\Shop;
use App\EventSubscriber\ShopLoader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class MediaCleanCommand extends Command
{
    protected static $defaultName = 'app:media:clean';
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var ShopLoader
     */
    private ShopLoader $shopLoader;
    /**
     * @var Filesystem
     */
    private Filesystem $filesystem;

    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader, Filesystem $filesystem)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->filesystem    = $filesystem;
        $this->shopLoader    = $shopLoader;
    }

    protected function configure()
    {
        $this
            ->setDescription('Handles media batch actions.')

            ->setHelp('This command handles media batch actions...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Media cleaner',
            '============',
            '',
        ]);

        $mediaList = $this->entityManager->getRepository(Media::class)->findBy(['status' => Media::STATUS_DELETED]);

        foreach ($mediaList as $media) {
            $this->shopLoader->setShop($media->getShop());
            $files = [$this->shopLoader->getPublicPath().$media->getFullPath()] + array_map(
                    function($elem) {
                        return $this->shopLoader->getPublicPath().$elem;
                    },
                    $media->getThumbnails(array_keys($this->shopLoader->getConfigItem('image_sizes')))
                );

            $this->filesystem->remove($files);
            $this->entityManager->remove($media);
        }

        $this->entityManager->flush();

        $output->writeln('Batch handled');
        return 0;
    }
}