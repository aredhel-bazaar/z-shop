<?php

namespace App\Command;

use App\Entity\Setting;
use App\Entity\Shop;
use App\Entity\Task;
use App\EventSubscriber\ShopLoader;
use App\Service\SettingsHolder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Filesystem;

class CreateShopCommand extends Command
{
    protected static $defaultName = 'app:create-shop';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var ShopLoader
     */
    private $shopLoader;

    public function __construct(EntityManagerInterface $entityManager, Filesystem $filesystem, ShopLoader $shopLoader, SettingsHolder $settingsHolder)
    {
        parent::__construct();

        $this->entityManager  = $entityManager;
        $this->filesystem     = $filesystem;
        $this->shopLoader     = $shopLoader;
        $this->settingsHolder = $settingsHolder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a new shop.')

            ->setHelp('This command allows you to create a new shop...')
            ->addArgument('domain', InputArgument::REQUIRED, 'Shop domain')
            ->addArgument('name', InputArgument::REQUIRED, 'Name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $shop = $this->entityManager->getRepository(Shop::class)->find($input->getArgument('domain'));

        if ($shop) {
            throw new \Exception("Shop already exists");
        }

        $shop = new Shop();
        $shop->setStatus(Shop::STATUS_OPENED)
            ->setDomain($input->getArgument('domain'))
            ->setName($input->getArgument('name'))
            ->setSecure(false)
            ->setDiskUsage(0)
        ;

        

        $setting = new Setting();
        $setting
            ->setId('logo')
            ->setType('string')
            ->setValue('')
        ;
        $shop->addSetting($setting);

        $setting = new Setting();
        $setting
            ->setId('meta')
            ->setType('array')
            ->setValue(json_encode(['title' => $shop->getName(), 'description' => '', 'keywords' => '']))
        ;
        $shop->addSetting($setting);

        //-- Creation of the folder
        $shopBasePath = $this->shopLoader->getBasePathFromDomain($input->getArgument('domain'));
        if ($this->filesystem->exists($shopBasePath)) {
            $question = $this->getHelper('question');
            if ($question->ask($input, $output, new ConfirmationQuestion("Folder already exists. Remove ?", false))) {
                $this->filesystem->remove($shopBasePath);
                // $output->writeln("Aborting");
                // exit;
            }
        }

        if (!$this->filesystem->exists($shopBasePath)) {
            $this->filesystem->mkdir($shopBasePath);
        }

        $this->entityManager->persist($shop);

        $task = new Task();
        $task->setCommand('app:check-disk-usage');
        $task->setRecurrenceInterval(new \DateInterval('PT1H'));
        $task->setExecutionDatetime(new \DateTime());
        $this->entityManager->persist($task);

        $this->entityManager->flush();


        $output->writeln("Shop {$input->getArgument('domain')} created.");
        return 0;
    }
}
