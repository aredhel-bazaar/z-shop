<?php

namespace App\Command;

use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaskManager extends Command
{
    protected static $defaultName = 'app:taskManager';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var TaskRepository
     */
    private $taskRepository;
    /**
     * @var string
     */
    private $name;

    /**
     * @param EntityManagerInterface $entityManager
     * @param TaskRepository         $taskRepository
     * @param LoggerInterface        $logger
     * @param string|null            $name
     */
    public function __construct(EntityManagerInterface $entityManager, TaskRepository $taskRepository, LoggerInterface $logger, string $name = null)
    {
        $this->entityManager  = $entityManager;
        $this->logger         = $logger;
        $this->taskRepository = $taskRepository;

        parent::__construct($name);
        $this->name = $name;
    }

    protected function configure()
    {
        $this
            ->setDescription('Task manager.')
            ->setHelp('This command acts as a service, handling platform tasks.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Task manager is up and running !");
        $this->logger->debug("Task manager is up and running !");

        while (true) {
            $this->heartbeat();

            $task = $this->taskRepository->getNextTask();
            if (!$task) {
                continue;
            }

            $output->writeln(sprintf("<info>Starting new task : [%s] %s</info>", $task->getId(), $task->getCommand()));
            $this->logger->debug(sprintf("Starting new task :[%s] %s", $task->getId(), $task->getCommand()));
            shell_exec("./bin/console app:task {$task->getId()} -q &");
        }
    }

    private function heartbeat()
    {
        usleep(500000);
    }
}
