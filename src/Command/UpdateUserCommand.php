<?php

namespace App\Command;

use App\Entity\Shop;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UpdateUserCommand extends Command
{
    protected static $defaultName = 'app:update-user';
    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager   = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Updates an existing user.')

            ->setHelp('This command allows you to update a user...')
            ->addOption('username', null, InputOption::VALUE_REQUIRED, 'The username of the user.')
            ->addOption('password', null, InputOption::VALUE_REQUIRED, 'New password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'User Updator',
            '============',
            '',
        ]);

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $input->getOption('username')]);

        if (!$user) {
            $output->writeln('<error>User does not exist.</error>');
            return 1;
        }

        $output->writeln('Username: '.$input->getOption('username'));
        $output->writeln('Password: '.str_repeat('*', mb_strlen($input->getOption('password'))));
        $output->writeln('');

        $user->setSalt(bin2hex(openssl_random_pseudo_bytes(16)))
            ->setPassword($this->passwordEncoder->encodePassword($user, $input->getOption('password')))
            ->setLanguage('fr')
            ->setStatus(1)
            ->setTimezone('Europe/Paris')
        ;

        $this->entityManager->flush();

        $output->writeln('User successfully updated!');
        return 0;
    }
}
