<?php

namespace App\Command;

use App\Entity\Shop;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';
    /**
     * @var bool
     */
    private bool $requirePassword;
    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager, bool $requirePassword = false)
    {
        parent::__construct();

        $this->requirePassword = $requirePassword;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager   = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a new user.')

            ->setHelp('This command allows you to create a user...')
            ->addOption('username', null, InputOption::VALUE_REQUIRED, 'The username of the user.')
            ->addOption('password', null, InputOption::VALUE_REQUIRED, 'User password')
            ->addOption('shop', null, InputOption::VALUE_REQUIRED, 'Shop')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['domain' => $input->getOption('shop')]);

        $output->writeln('Username: '.$input->getOption('username'));
        $output->writeln('Password: '.str_repeat('*', mb_strlen($input->getOption('password'))));
        $output->writeln('Shop domain: '.$shop->getDomain());
        $output->writeln('');

        $user = new User();
        $user->setUsername($input->getOption('username'))
            ->setSalt(bin2hex(openssl_random_pseudo_bytes(16)))
            ->setPassword($this->passwordEncoder->encodePassword($user, $input->getOption('password')))
            ->setLanguage('fr')
            ->setStatus(1)
            ->setTimezone('Europe/Paris')
            ->setShop($shop)
        ;

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $output->writeln('User successfully generated!');
        return 0;
    }
}
