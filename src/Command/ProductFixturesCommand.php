<?php

namespace App\Command;

use App\Entity\Brand;
use App\Entity\Media;
use App\Entity\Product;
use App\EventSubscriber\ShopLoader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ProductFixturesCommand extends Command
{
    protected static $defaultName = 'app:products:fixtures';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ShopLoader
     */
    private $shopLoader;

    /**
     * ProductFixturesCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ShopLoader             $shopLoader
     */
    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->shopLoader    = $shopLoader;
    }

    protected function configure()
    {
        $this
            ->setDescription('Populate database with "some" data.')

            ->addOption('number', '', InputOption::VALUE_REQUIRED, '', 100)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $media = $this->entityManager->getRepository(Media::class)->findAll();

        //-- Filter
        foreach ($media as $key => $obj) {
            if (!$obj->supportsThumbnail()) {
                unset($media[$key]);
            }
        }
        $media = array_values($media);
        //-- Filter

        $brands = $this->entityManager->getRepository(Brand::class)->findAll();
        if (count($brands) < 200) {
            for ($i=count($brands); $i<200; $i++) {
                $brand = new Brand();
                $brand->setName(substr(str_shuffle(MD5(microtime())), 0, 10))
                    ->setShop($this->shopLoader->getLoadedShop())
                ;

                $this->entityManager->persist($brand);
                $brands[] = $brand;
            }
            $this->entityManager->flush();
        }

        for ($i=0; $i<=$input->getOption('number'); $i++) {
            $nbMedias = mt_rand(0, 4);
            $subMedias = [];
            for ($j=0; $j<$nbMedias; $j++) {
                $selectedMedia = $media[rand(0,count($media)-1)];
                $subMedias[$selectedMedia->getId()] = $selectedMedia;
            }

            $brand = $brands[array_rand($brands)];

            $product = new Product();
            $product
                ->setShop($this->shopLoader->getLoadedShop())
                ->setName("Product ".substr(str_shuffle(MD5(microtime())), 0, 10))
                ->setPrice(mt_rand(2, 1000) / (max(10 * mt_rand(0, 3), 1)))
                ->setMainMedia($media[rand(0,count($media)-1)])
                ->setMedias($subMedias)
                ->setSku(substr(str_shuffle(MD5(microtime())), 0, 10))
                ->setEan(substr(str_shuffle(MD5(microtime())), 0, 13))
                ->setMpn(substr(str_shuffle(MD5(microtime())), 0, 12))
                ->setStock(rand(0, 1000))
                ->setBrand($brand)
            ;

            $this->entityManager->persist($product);
            if ($i>0 && $i%10000 == 0) {
                $this->entityManager->flush();
            }

            $output->write("\r$i/{$input->getOption('number')} products created.");
            usleep(10000);
        }

        if ($this->entityManager->getUnitOfWork()->hasPendingInsertions()) {
            $this->entityManager->flush();
        }
        $output->writeln("");
        return 0;
    }
}
