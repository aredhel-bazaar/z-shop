<?php

namespace App\Command;

use App\Entity\Shop;
use App\EventSubscriber\ShopLoader;
use App\Service\DashboardData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DashboardDataCommand extends Command
{
    protected static $defaultName = 'app:dashboard:reload';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ShopLoader
     */
    private $shopLoader;
    /**
     * @var DashboardData
     */
    private $dashboardData;

    /**
     * DashboardDataCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ShopLoader             $shopLoader
     * @param DashboardData          $dashboardData
     */
    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader, DashboardData $dashboardData)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->shopLoader    = $shopLoader;
        $this->dashboardData = $dashboardData;
    }

    protected function configure()
    {
        $this
            ->setDescription('Reloads the data used in the different charts of the dashboard')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->entityManager->getRepository(Shop::class)->findBy(['status' => Shop::STATUS_OPENED]) as $shop) {
            $this->shopLoader->setShop($shop);
            $this->dashboardData->buildCache();
        }
        return 0;
    }
}
