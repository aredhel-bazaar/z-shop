<?php

namespace App\Command;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaskHandlerCommand extends Command
{
    protected static $defaultName = 'app:task';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TaskRepository
     */
    private $taskRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var string|null
     */
    private $name;

    public function __construct(EntityManagerInterface $entityManager, TaskRepository $taskRepository, LoggerInterface $logger, string $name = null)
    {
        $this->entityManager  = $entityManager;
        $this->taskRepository = $taskRepository;
        $this->logger         = $logger;
        $this->name           = $name;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Handle a given task.')
            ->setHelp('This command allows you to run a given task...')

            ->addArgument('task', InputArgument::REQUIRED, 'ID of the task')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $task = $this->entityManager->getRepository(Task::class)->find($input->getArgument('task'));

        if (is_null($task)) {
            $output->writeln(sprintf("<error>Task not found : %s</error>", $task->getCommand()));
            $this->logger->error(sprintf("Task not found : %s", $task->getCommand()));

            return 1;
        }

        $output->writeln(sprintf("<info>Starting new task : %s</info>", $task->getCommand()));
        $this->logger->debug(sprintf("Starting new task : %s", $task->getCommand()));

        if ($task->getRecurrenceCount() > 0) {
            $task->setRecurrenceCount($task->getRecurrenceCount() - 1);
        }

        try {
            $taskCommand = $this->getApplication()->find($task->getCommand());
            $taskInput   = new ArrayInput($task->getPayload());

            $task->setStatus(Task::STATUS_RUNNING);
            $this->entityManager->flush();

            $returnCode  = $taskCommand->run($taskInput, $output);

            $this->logger->debug(sprintf("Return code from task : %s", $returnCode));
            $task->setStatus(Task::STATUS_SUCCESS);
        } catch (\Exception $e) {
            $this->logger->error(sprintf("Error while executing task command %s : %s", $task->getCommand(), $e->getMessage()));
            $output->writeln(sprintf("<error>Error while executing task command %s : %s</error>", $task->getCommand(), $e->getMessage()));
            $task->setStatus(Task::STATUS_ERROR);
        }

        if ($task->getRecurrenceCount() === 0) {
            if ($task->getStatus() === Task::STATUS_SUCCESS) {
                $this->entityManager->remove($task);
            }
        } else {
            if ($task->getRecurrenceInterval()) {
                $lastExecutionDatetime = clone $task->getExecutionDatetime();
                $nextExecutionDatetime = $lastExecutionDatetime->add($task->getRecurrenceInterval());
                if ($nextExecutionDatetime < new \Datetime()) {
                    $nextExecutionDatetime = (new \DateTime())->add($task->getRecurrenceInterval());
                }
                $task->setExecutionDatetime($nextExecutionDatetime);
                $task->setStatus(Task::STATUS_PENDING);
            }
        }

        $this->entityManager->flush();
        return 0;
    }
}
