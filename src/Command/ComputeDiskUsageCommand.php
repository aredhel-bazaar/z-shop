<?php

namespace App\Command;

use App\Entity\Shop;
use App\EventSubscriber\ShopLoader;
use App\Service\FilesystemHelper;
use App\Service\TextHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ComputeDiskUsageCommand extends Command
{
    protected static $defaultName = 'app:compute-disk-usage';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var ShopLoader
     */
    private $shopLoader;

    /**
     * ComputeDiskUsageCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param Filesystem             $filesystem
     * @param ShopLoader             $shopLoader
     */
    public function __construct(EntityManagerInterface $entityManager, Filesystem $filesystem, ShopLoader $shopLoader)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->filesystem    = $filesystem;
        $this->shopLoader    = $shopLoader;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setDescription('Compute and save disk space usage for each opened shop.')
            ->setHelp('This command compute and save disk space usage for each opened shop.')
            ->addOption('shop', null, InputOption::VALUE_REQUIRED, 'Shop')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('shop')) {
            $shops = $this->entityManager->getRepository(Shop::class)->findBy(['status' => Shop::STATUS_OPENED]);

            foreach ($shops as $shop) {
                $this->shopLoader->setShop($shop);
                $this->computeShopDirectorySize($shop);
                $output->writeln("Shop {$shop->getDomain()} : \t".TextHelper::humanFilesize($shop->getDiskUsage()));
            }
        } else {
            $this->computeShopDirectorySize($this->shopLoader->getLoadedShop());
        }

        $this->entityManager->flush();

        return 0;
    }

    /**
     * @param $shop
     */
    protected function computeShopDirectorySize($shop)
    {
        $shop->setDiskUsage(FilesystemHelper::getDirectorySize($this->shopLoader->getPublicPath()));
    }
}
