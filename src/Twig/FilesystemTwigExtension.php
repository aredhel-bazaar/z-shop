<?php

namespace App\Twig;

use App\EventSubscriber\ShopLoader;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class FilesystemTwigExtension extends AbstractExtension
{
    /**
     * @var ShopLoader
     */
    private $shopLoader;

    public function __construct(ShopLoader $shopLoader)
    {
        $this->shopLoader = $shopLoader;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('absolutePath', [$this, 'absolutePath']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('filesize', [$this, 'filesize'])
        ];
    }

    public function absolutePath(string $path)
    {
        return $this->shopLoader->getPublicPath().$path;
    }

    public function filesize(string $path)
    {
        return @filesize($path);
    }
}