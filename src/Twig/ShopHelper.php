<?php

namespace App\Twig;

use App\Entity\Setting;
use App\EventSubscriber\ShopLoader;
use App\EventSubscriber\ThemeLoader;
use App\Repository\ProductRepository;
use App\Service\SettingsHolder;

class ShopHelper
{
    private $name;
    private $nbProducts;

    /**
     * @var ShopLoader
     */
    private $shopLoader;
    /**
     * @var ThemeLoader
     */
    private $themeLoader;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var SettingsHolder
     */
    private $settingsHolder;

    /**
     * ShopHelper constructor.
     *
     * @param ShopLoader        $shopLoader
     * @param ThemeLoader       $themeLoader
     * @param ProductRepository $productRepository
     */
    public function __construct(ShopLoader $shopLoader, ThemeLoader $themeLoader, ProductRepository $productRepository)
    {
        $this->shopLoader        = $shopLoader;
        $this->themeLoader       = $themeLoader;
        $this->productRepository = $productRepository;
    }

    public function getName()
    {
        if (is_null($this->name)) {
            $this->name = $this->shopLoader->getLoadedShop()->getName();
        }

        return $this->name;
    }

    public function getNbProducts()
    {
        if (is_null($this->nbProducts)) {
            $this->nbProducts = $this->productRepository->countAll();
        }

        return $this->nbProducts;
    }

    public function getSettings()
    {
        if (is_null($this->settingsHolder)) {
            $this->settingsHolder = new SettingsHolder();
            $this->settingsHolder->load($this->shopLoader->getLoadedShop()->getSettings());
        }

        return $this->settingsHolder;
    }

    public function getTheme()
    {
        return $this->themeLoader->getLoadedTheme();
    }

    public function getQuota()
    {
        return $this->shopLoader->getConfigItem('quota');
    }

    public function getDiskUsage()
    {
        return $this->shopLoader->getLoadedShop()->getDiskUsage();
    }

    public function getQuotaUsage()
    {
        return round($this->getDiskUsage() / $this->getQuota(), 4);
    }

    public function getStatusLabel()
    {
        return $this->shopLoader->getLoadedShop()->getStatus();
    }
}
