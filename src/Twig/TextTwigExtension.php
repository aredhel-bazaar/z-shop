<?php

namespace App\Twig;

use App\Service\TextHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TextTwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('loremipsum', [TextHelper::class, 'getLoremIpsum']),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('humanFilesize', [TextHelper::class, 'humanFilesize'])
        ];
    }
}