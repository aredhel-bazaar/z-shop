<?php

namespace App\EventSubscriber;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CommandEventSubscriber implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            ConsoleEvents::COMMAND => [
                'alterCommandDefinition', 32
            ]
        ];
    }

    public function alterCommandDefinition(ConsoleCommandEvent $event)
    {
        $event->getCommand()->getDefinition()->addOption(new InputOption('shop', null, InputOption::VALUE_REQUIRED));
        $event->getInput()->bind($event->getCommand()->getDefinition());
        $event->getInput()->validate();

        if (!$event->getInput()->getOption('shop')) {
            if (!$event->getOutput()->isQuiet()) {
                $event->getOutput()->writeln("<warning>Command issued without '--shop' option. Outcome may be uncertain.</warning>");

                // if ($event->getInput()->isInteractive()) {
                //     $question = $event->getCommand()->getHelper('question');
                //     if (!$question->ask($event->getInput(), $event->getOutput(), new ConfirmationQuestion("Are you sure this is what you want ?", false))) {
                //         $event->getOutput()->writeln("Ouf, that was a close one.");
                //         $event->disableCommand();
                //     }
                // }
            }
        }
    }
}
