<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Loader\FilesystemLoader;

class ThemeLoader implements EventSubscriberInterface
{
    /**
     * @var FilesystemLoader
     */
    private $filesystemLoader;
    /**
     * @var string
     */
    private $projectDir;
    /**
     * @var string
     */
    private $theme;
    /**
     * @var string
     */
    private $shopId;
    /**
     * @var string
     */
    private $adminTheme;
    /**
     * @var string
     */
    private $loadedTheme;
    /**
     * @var array|false
     */
    private $themes;

    public function __construct(FilesystemLoader $filesystemLoader, string $projectDir, string $shopId, string $theme, string $adminTheme)
    {
        $this->filesystemLoader = $filesystemLoader;
        $this->projectDir       = $projectDir;
        $this->shopId           = $shopId;
        $this->theme            = $theme;
        $this->adminTheme       = $adminTheme;

        $this->getThemes();
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['addTwigPaths']
        ];
    }

    /**
     * @param RequestEvent $event
     *
     * @throws \Twig\Error\LoaderError
     */
    public function addTwigPaths(RequestEvent $event)
    {
        $this->loadedTheme = $this->theme;
        if ($event->getRequest()->attributes->get('_firewall_context') === 'security.firewall.map.context.back') {
            $this->loadedTheme = $this->adminTheme;
        }

        if ($this->loadedTheme !== 'default') {
            $this->filesystemLoader->prependPath($this->projectDir.'/templates/themes/default/tpl');
        }

        $this->filesystemLoader->prependPath($this->projectDir.'/templates/themes/'.$this->loadedTheme.'/tpl');

        if ($this->loadedTheme !== 'default') {
            try {
                $this->filesystemLoader->prependPath($this->projectDir.'/shop/'.$this->shopId.'/themes/default/tpl');
            } catch (\Exception $e) { /* Shop does not have a customized theme */ }
        }

        try {
            $this->filesystemLoader->prependPath($this->projectDir.'/shop/'.$this->shopId.'/themes/'.$this->loadedTheme.'/tpl');
        } catch (\Exception $e) { /* Shop does not have a customized theme */ }
    }

    /**
     * @return string
     */
    public function getLoadedTheme()
    {
        return $this->loadedTheme;
    }

    public function getFrontTheme()
    {
        return $this->theme;
    }

    public function getAdminTheme()
    {
        return $this->adminTheme;
    }

    /**
     * @return string[]
     */
    public function getThemes()
    {
        if (null === $this->themes) {
            $this->themes = array_map(function($theme) {
                return basename($theme);
            }, glob($this->projectDir.'/templates/themes/*'));
        }

        return $this->themes;
    }
}
