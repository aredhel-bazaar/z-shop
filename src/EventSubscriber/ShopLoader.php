<?php

namespace App\EventSubscriber;

use App\Entity\Shop;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Yaml\Yaml;

class ShopLoader implements EventSubscriberInterface
{
    public const CONFIG_ITEMS = [
        'theme'       => 'parameters',
        'admin_theme' => 'parameters',
        'image_sizes' => 'parameters'
    ];

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Shop
     */
    private $shop;
    /**
     * @var string
     */
    private $projectDir;
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var array
     */
    private $config;

    /**
     * ShopLoader constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param string                 $projectDir
     * @param ParameterBagInterface  $parameterBag
     */
    public function __construct(EntityManagerInterface $entityManager, string $projectDir, ParameterBagInterface $parameterBag)
    {
        $this->entityManager = $entityManager;
        $this->projectDir    = $projectDir;
        $this->parameterBag  = $parameterBag;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['requestLoadShop', 2048],
            ConsoleEvents::COMMAND => ['consoleLoadShop', 16],
        ];
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop)
    {
        $_ENV['SHOP_DOMAIN'] = $shop->getDomain();
        $this->shop          = $shop;
    }

    public function loadShopFromName(string $shopName)
    {
        $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['domain' => $shopName]);

        if (is_null($shop)) {
            //-- No shop found.
            throw new AccessDeniedException("No shop found");
        }

        if ($shop->isClosed()) {
            //-- Shop is closed. Same as if it did not exist.
            throw new AccessDeniedException("Shop is closed");
        }

        $this->setShop($shop);
    }

    /**
     * @param RequestEvent $event
     */
    public function requestLoadShop(RequestEvent $event)
    {
        $this->loadShopFromName($event->getRequest()->getHost());
    }

    /**
     * @param ConsoleCommandEvent $event
     */
    public function consoleLoadShop(ConsoleCommandEvent $event)
    {
        if (!$event->getInput()->hasOption('shop') || !$event->getInput()->getOption('shop')) {
            return;
        }

        $this->loadShopFromName($event->getInput()->getOption('shop'));
    }

    /**
     * @return Shop
     */
    public function getLoadedShop()
    {
        return $this->shop;
    }

    /**
     * @return string
     */
    public function getBasePath()
    {
        return "{$this->projectDir}/shop/{$this->getLoadedShop()->getDomain()}/";
    }

    /**
     * @param string $domain
     *
     * @return string
     */
    public function getBasePathFromDomain(string $domain)
    {
        return "{$this->projectDir}/shop/{$domain}/";
    }

    /**
     * @param bool $relative
     *
     * @return string
     */
    public function getPublicPath(bool $relative = false)
    {
        return (!$relative ? $this->getBasePath() : '/shop/'.$this->getLoadedShop()->getDomain().'/').'public';
    }

    /**
     * @param string{'quota'} $config
     *
     * @return mixed
     */
    public function getConfigItem(string $config)
    {
        return $this->parameterBag->get($config);
    }

    /**
     * @return array
     */
    public function getShopConfig()
    {
        if (null === $this->config) {
            if (!file_exists($this->getBasePath().'/config/config.yaml')) {
                if (!is_dir($this->getBasePath().'/config')) {
                    mkdir($this->getBasePath().'/config');
                }
                touch($this->getBasePath().'/config/config.yaml');
            }
            
            $this->config = yaml_parse_file($this->getBasePath().'/config/config.yaml');
        }

        return $this->config;
    }

    /**
     * @param string[] $newConfig
     * @param bool     $replace
     *
     * @return bool
     */
    public function saveConfig(array $newConfig, $replace = false)
    {
        $config = [];
        if (false === $replace) {
            $config = $this->getShopConfig();
        }

        foreach ($newConfig as $item => $value) {
            $config[self::CONFIG_ITEMS[$item]][$item] = $value;
        }

        $config['parameters']['shop_id'] = $this->shop->getDomain();

        file_put_contents($this->getBasePath().'/config/config.yaml', Yaml::dump($config));
        // exec("make --directory=".escapeshellarg($this->projectDir)." --file=".escapeshellarg($this->projectDir."/Makefile")." cache-clear shop=".escapeshellarg($this->shop->getDomain()), $clearOutput);
        // exec("make --directory=".escapeshellarg($this->projectDir)." --file=".escapeshellarg($this->projectDir."/Makefile")." cache-warmup shop=".escapeshellarg($this->shop->getDomain()), $warmupOutput);

        return true;
    }
}
