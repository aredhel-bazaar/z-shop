<?php

namespace App\Repository;

use App\Entity\Media;
use App\Entity\Task;
use App\EventSubscriber\ShopLoader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;

class TaskRepository extends AbstractRepository
{
    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader)
    {
        $this->repository    = $entityManager->getRepository(Task::class);
        $this->shopLoader    = $shopLoader;
        $this->classMetadata = $entityManager->getClassMetadata(Task::class);
    }

    /**
     * @return Task|null
     */
    public function getNextTask()
    {
        $task = null;

        try {
            $task = $this->repository->createQueryBuilder('task')
                ->select('task')
                ->where('task.executionDatetime <= :now')
                ->andWhere('task.status = 0')
                ->setParameter('now', new \DateTime())
                ->orderBy('task.executionDatetime')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult()
            ;
        }
        catch (NoResultException $e) {}
        catch (NonUniqueResultException $e) {}

        return $task;
    }
    /**
     * @param array $filters
     *
     * @return int
     */
    public function countAll($filters = [])
    {
        $parameters = [new Parameter('id', $this->shopLoader->getLoadedShop()->getId())];
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('task');

        $builder
            ->select('COUNT(task.id)')
            ->where('task.shop = :id')
        ;

        $parameters = array_merge($parameters, $this->getQueryFilters($builder, array_filter($filters ?? [])));

        try {
            $nbProducts = (int)$builder
                ->setParameters(new ArrayCollection($parameters))
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }

        return $nbProducts;
    }

    /**
     * @param $fields
     * @param $offset
     * @param $limit
     *
     * @return mixed
     */
    public function searchForTasks($fields, $offset, $limit)
    {
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('task');

        $parameters = [
            new Parameter('id', $this->shopLoader->getLoadedShop()->getId())
        ];
        $builder
            ->where('task.shop = :id OR task.shop IS NULL')
        ;

        $parameters = array_merge($parameters, $this->getQueryFilters($builder, array_filter($fields ?? [])));

        $builder->setParameters(new ArrayCollection($parameters));

        return $builder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $builder
     * @param $fields
     *
     * @return array
     */
    private function getQueryFilters(QueryBuilder $builder, array $fields)
    {
        $parameters = [];

        return $parameters;
    }
}