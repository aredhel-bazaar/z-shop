<?php

namespace App\Repository;

use App\EventSubscriber\ShopLoader;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ObjectRepository;

abstract class AbstractRepository implements ObjectRepository, Selectable
{
    /**
     * @var EntityRepository
     */
    protected $repository;
    /**
     * @var ClassMetadata
     */
    protected $classMetadata;
    /**
     * @var ShopLoader
     */
    protected $shopLoader;

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     *
     * @return object|null The object.
     */
    public function find($id)
    {
        return $this->repository->findOneBy([$this->classMetadata->getSingleIdentifierFieldName() => $id]);
    }

    /**
     * Finds all objects in the repository.
     *
     * @return object[] The objects.
     */
    public function findAll()
    {
        return $this->findBy([]);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return object[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        if (!isset($criteria['shop']) && $this->classMetadata->hasAssociation('shop')) {
            $criteria['shop'] = $this->shopLoader->getLoadedShop();
        }

        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param mixed[] $criteria The criteria.
     *
     * @return object|null The object.
     */
    public function findOneBy(array $criteria)
    {
        if (!isset($criteria['shop']) && $this->classMetadata->hasAssociation('shop')) {
            $criteria['shop'] = $this->shopLoader->getLoadedShop();
        }

        return $this->repository->findOneBy($criteria);
    }

    /**
     * Returns the class name of the object managed by the repository.
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->repository->getClassName();
    }

    /**
     * Selects all elements from a selectable that match the expression and
     * returns a new collection containing these elements.
     *
     * @return Collection
     *
     * @psalm-return Collection<TKey,T>
     */
    public function matching(Criteria $criteria)
    {
        return $this->repository->matching($criteria);
    }
}