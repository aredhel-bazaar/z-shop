<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Shop;
use App\EventSubscriber\ShopLoader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ShopRepository
 * @package App\Repository
 */
class ShopRepository extends AbstractRepository
{
    /**
     * ProductRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ShopLoader             $shopLoader
     */
    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader)
    {
        $this->shopLoader    = $shopLoader;
        $this->repository    = $entityManager->getRepository(Shop::class);
        $this->classMetadata = $entityManager->getClassMetadata(Shop::class);
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function countAll($filters = [])
    {
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('shop');

        $builder
            ->select('COUNT(shop.id)')
        ;

        $parameters = $this->getQueryFilters($builder, array_filter($filters ?? []));

        try {
            $nbShops = (int)$builder
                ->setParameters(new ArrayCollection($parameters))
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }

        return $nbShops;
    }

    /**
     * @param $fields
     * @param $offset
     * @param $limit
     *
     * @return mixed
     */
    public function searchForShops($fields, $offset, $limit)
    {
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('shop');

        $parameters = $this->getQueryFilters($builder, array_filter($fields ?? []));

        if (!empty($parameters)) {
            $builder->setParameters(new ArrayCollection($parameters));
        }

        return $builder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $builder
     * @param $fields
     *
     * @return array
     */
    private function getQueryFilters(QueryBuilder $builder, array $fields)
    {
        $parameters = [];
        if (!empty($fields)) {
            if (count($fields) > 1 || count($fields) && !isset($fields['search'])) {
                foreach ($fields as $field => $value) {
                    if ($field === 'search') {
                        continue;
                    }

                    if (!$this->classMetadata->hasField($field)) {
                        continue;
                    }

                    $builder->andWhere("shop.{$field} LIKE :{$field}");
                    $parameters[] = new Parameter($field, str_replace('_', '\\_', "%{$value}%"));
                }
            } else {
                $builder->andWhere('shop.name LIKE :search 
                    OR shop.domain LIKE :search
                ');

                $parameters[] = new Parameter('search', str_replace('_', '\\_', "%{$fields['search']}%"));
            }
        }

        return $parameters;
    }
}
