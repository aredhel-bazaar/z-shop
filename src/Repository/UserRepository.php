<?php

namespace App\Repository;

use App\Entity\User;
use App\EventSubscriber\ShopLoader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends AbstractRepository
{
    /**
     * ProductRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ShopLoader             $shopLoader
     */
    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader)
    {
        $this->shopLoader    = $shopLoader;
        $this->repository    = $entityManager->getRepository(User::class);
        $this->classMetadata = $entityManager->getClassMetadata(User::class);
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function countAll($filters = [])
    {
        $parameters = [new Parameter('id', $this->shopLoader->getLoadedShop()->getId())];
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('user');

        $builder
            ->select('COUNT(user.id)')
            ->where('user.shop = :id')
        ;

        $parameters = array_merge($parameters, $this->getQueryFilters($builder, array_filter($filters ?? [])));

        try {
            $nbProducts = (int)$builder
                ->setParameters(new ArrayCollection($parameters))
                ->groupBy('user.shop')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }

        return $nbProducts;
    }

    /**
     * @param $fields
     * @param $offset
     * @param $limit
     *
     * @return mixed
     */
    public function searchForUsers($fields, $offset, $limit)
    {
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('user');

        $parameters = [
            new Parameter('id', $this->shopLoader->getLoadedShop()->getId())
        ];
        $builder
            ->where('user.shop = :id')
        ;

        $parameters = array_merge($parameters, $this->getQueryFilters($builder, array_filter($fields ?? [])));

        $builder->setParameters(new ArrayCollection($parameters));

        return $builder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $builder
     * @param $fields
     *
     * @return array
     */
    private function getQueryFilters(QueryBuilder $builder, array $fields)
    {
        $parameters = [];

        return $parameters;
    }
}
