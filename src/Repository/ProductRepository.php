<?php

namespace App\Repository;

use App\Entity\Product;
use App\EventSubscriber\ShopLoader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ProductRepository
 * @package App\Repository
 */
class ProductRepository extends AbstractRepository
{
    /**
     * ProductRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ShopLoader             $shopLoader
     */
    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader)
    {
        $this->shopLoader    = $shopLoader;
        $this->repository    = $entityManager->getRepository(Product::class);
        $this->classMetadata = $entityManager->getClassMetadata(Product::class);
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function countAll($filters = [])
    {
        $parameters = [new Parameter('id', $this->shopLoader->getLoadedShop()->getId())];
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('product');

        $builder
            ->select('COUNT(product.id)')
            ->where('product.shop = :id')
            ->andWhere('product.parent IS NULL');

        $parameters = array_merge($parameters, $this->getQueryFilters($builder, array_filter($filters ?? [])));

        try {
            $nbProducts = (int)$builder
                ->setParameters(new ArrayCollection($parameters))
                ->groupBy('product.shop')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }

        return $nbProducts;
    }

    /**
     * @param $fields
     * @param $offset
     * @param $limit
     *
     * @return mixed
     */
    public function searchForProducts($fields, $offset, $limit)
    {
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('product');

        $parameters = [
            new Parameter('id', $this->shopLoader->getLoadedShop()->getId())
        ];
        $builder
            ->where('product.shop = :id')
            ->andWhere('product.parent IS NULL');

        $parameters = array_merge($parameters, $this->getQueryFilters($builder, array_filter($fields ?? [])));

        $builder->setParameters(new ArrayCollection($parameters));

        return $builder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $builder
     * @param $fields
     *
     * @return array
     */
    private function getQueryFilters(QueryBuilder $builder, array $fields)
    {
        $parameters = [];
        if (!empty($fields)) {
            if (count($fields) > 1 || count($fields) && !isset($fields['search'])) {
                foreach ($fields as $field => $value) {
                    if ($field === 'search') {
                        continue;
                    }

                    if (!$this->classMetadata->hasField($field)) {
                        continue;
                    }

                    $builder->andWhere("product.{$field} LIKE :{$field}");
                    $parameters[] = new Parameter($field, str_replace('_', '\\_', "%{$value}%"));
                }
            } else {
                $builder->andWhere('product.name LIKE :search 
                    OR product.sku LIKE :search 
                    OR product.ean LIKE :search 
                    OR product.mpn LIKE :search
                ');

                $parameters[] = new Parameter('search', str_replace('_', '\\_', "%{$fields['search']}%"));
            }
        }

        return $parameters;
    }
}
