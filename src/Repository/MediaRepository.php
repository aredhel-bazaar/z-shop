<?php

namespace App\Repository;

use App\Entity\Media;
use App\EventSubscriber\ShopLoader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class MediaRepository extends AbstractRepository
{
    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader)
    {
        $this->shopLoader    = $shopLoader;
        $this->repository    = $entityManager->getRepository(Media::class);
        $this->classMetadata = $entityManager->getClassMetadata(Media::class);
    }

    public function findByPathStart(string $path)
    {
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('media');
        $builder
            ->where('media.shop = :id')
            ->andWhere('media.path LIKE :path')
            ->andWhere('media.status = :status')
            ->setParameters([
                'id' => $this->shopLoader->getLoadedShop()->getId(),
                'path' => "{$path}%",
                'status' => Media::STATUS_NORMAL
            ]);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @return object[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null) {
        if (!array_key_exists('status', $criteria)) {
            $criteria['status'] = Media::STATUS_NORMAL;
        }

        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }


}
