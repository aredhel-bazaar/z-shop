<?php

namespace App\Repository;

use App\Entity\Order;
use App\EventSubscriber\ShopLoader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;

class OrderRepository extends AbstractRepository
{
    public function __construct(EntityManagerInterface $entityManager, ShopLoader $shopLoader)
    {
        $this->shopLoader    = $shopLoader;
        $this->repository    = $entityManager->getRepository(Order::class);
        $this->classMetadata = $entityManager->getClassMetadata(Order::class);
    }

    /**
     * @param array $filters
     *
     * @return int
     */
    public function countAll($filters = [])
    {
        $parameters = [new Parameter('id', $this->shopLoader->getLoadedShop()->getId())];
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('o');

        $builder
            ->select('COUNT(o.id)')
            ->where('o.shop = :id')
        ;

        $parameters = array_merge($parameters, $this->getQueryFilters($builder, array_filter($filters ?? [])));

        try {
            $nbProducts = (int)$builder
                ->setParameters(new ArrayCollection($parameters))
                ->groupBy('o.shop')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }

        return $nbProducts;
    }

    /**
     * @param $fields
     * @param $offset
     * @param $limit
     *
     * @return mixed
     */
    public function searchForOrders($fields, $offset, $limit)
    {
        /** @var QueryBuilder $builder */
        $builder = $this->repository->createQueryBuilder('o');

        $parameters = [
            new Parameter('id', $this->shopLoader->getLoadedShop()->getId())
        ];
        $builder
            ->where('o.shop = :id')
        ;

        $parameters = array_merge($parameters, $this->getQueryFilters($builder, array_filter($fields ?? [])));

        $builder->setParameters(new ArrayCollection($parameters));

        return $builder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $builder
     * @param $fields
     *
     * @return array
     */
    private function getQueryFilters(QueryBuilder $builder, array $fields)
    {
        $parameters = [];

        return $parameters;
    }
}
