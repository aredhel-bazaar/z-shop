<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220426153135 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `brand` (shop_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `name` VARCHAR(255) NOT NULL, INDEX IDX_1C52F9584D16C4DD (shop_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `cart` (customer_id INT DEFAULT NULL, `id` BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', `creation_datetime` DATETIME DEFAULT NULL, `modification_datetime` DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_BA388B79395C3F3 (customer_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `category` (main_media INT DEFAULT NULL, shop_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `name` VARCHAR(255) NOT NULL, `lft` INT NOT NULL, `rgt` INT NOT NULL, `description` LONGTEXT NOT NULL, `path` VARCHAR(255) NOT NULL, INDEX IDX_64C19C1C970124A (main_media), INDEX IDX_64C19C14D16C4DD (shop_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `customer` (user_id INT DEFAULT NULL, shop_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `email` VARCHAR(192) NOT NULL, `first_name` VARCHAR(60) NOT NULL, `last_name` VARCHAR(60) NOT NULL, `creation_datetime` DATETIME NOT NULL, `modification_datetime` DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_81398E09A76ED395 (user_id), INDEX IDX_81398E094D16C4DD (shop_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `media` (shop_id INT DEFAULT NULL, user_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `path` VARCHAR(255) NOT NULL, `filename` VARCHAR(255) NOT NULL, `name` VARCHAR(255) NOT NULL, `status` INT NOT NULL, `creation_datetime` DATETIME DEFAULT NULL, `modification_datetime` DATETIME DEFAULT NULL, INDEX IDX_6A2CA10C4D16C4DD (shop_id), INDEX IDX_6A2CA10CA76ED395 (user_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (shop_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `total_ttc` DOUBLE PRECISION NOT NULL, `creation_datetime` DATETIME NOT NULL, `modification_datetime` DATETIME DEFAULT NULL, INDEX IDX_F52993984D16C4DD (shop_id), INDEX IDX_F52993989395C3F3 (customer_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order_product` (order_id INT DEFAULT NULL, main_media INT DEFAULT NULL, brand_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `name` VARCHAR(100) NOT NULL, `price` DOUBLE PRECISION NOT NULL, `sku` VARCHAR(30) DEFAULT NULL, `npm` VARCHAR(30) DEFAULT NULL, `ean` VARCHAR(30) DEFAULT NULL, `quantity` INT NOT NULL, INDEX IDX_2530ADE68D9F6D38 (order_id), INDEX IDX_2530ADE6C970124A (main_media), INDEX IDX_2530ADE644F5D008 (brand_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `product` (shop_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, main_media INT DEFAULT NULL, brand_id INT DEFAULT NULL, category_id INT DEFAULT NULL, carts_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', `id` INT AUTO_INCREMENT NOT NULL, `name` VARCHAR(100) NOT NULL, `price` DOUBLE PRECISION NOT NULL, `sku` VARCHAR(30) DEFAULT NULL, `mpn` VARCHAR(30) DEFAULT NULL, `ean` VARCHAR(30) DEFAULT NULL, `stock` INT NOT NULL, `creation_datetime` DATETIME DEFAULT NULL, `modification_datetime` DATETIME DEFAULT NULL, INDEX IDX_D34A04AD4D16C4DD (shop_id), INDEX IDX_D34A04AD727ACA70 (parent_id), INDEX IDX_D34A04ADC970124A (main_media), INDEX IDX_D34A04AD44F5D008 (brand_id), INDEX IDX_D34A04AD12469DE2 (category_id), INDEX IDX_D34A04ADBCB5C6F5 (carts_id), INDEX sku_idx (sku), INDEX mpn_idx (mpn), INDEX ean_idx (ean), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_media (product_id INT NOT NULL, media_id INT NOT NULL, INDEX IDX_CB70DA504584665A (product_id), INDEX IDX_CB70DA50EA9FDD75 (media_id), PRIMARY KEY(product_id, media_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_category (product_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_CDFC73564584665A (product_id), INDEX IDX_CDFC735612469DE2 (category_id), PRIMARY KEY(product_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `setting` (shop_id INT NOT NULL, `id` VARCHAR(30) NOT NULL, `type` VARCHAR(30) NOT NULL, `value` LONGTEXT NOT NULL, INDEX IDX_9F74B8984D16C4DD (shop_id), PRIMARY KEY(`id`, shop_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `shop` (parent_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `domain` VARCHAR(50) NOT NULL, `name` VARCHAR(255) NOT NULL, `status` INT NOT NULL, `secure` TINYINT(1) NOT NULL, `disk_usage` DOUBLE PRECISION NOT NULL, INDEX IDX_AC6A4CA2727ACA70 (parent_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `task` (shop_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `command` VARCHAR(50) NOT NULL, `payload` JSON NOT NULL, `status` INT NOT NULL, `recurrence_count` INT NOT NULL, `recurrence_interval` VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:dateinterval)\', `execution_datetime` DATETIME NOT NULL, `creation_datetime` DATETIME NOT NULL, INDEX IDX_527EDB254D16C4DD (shop_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (shop_id INT DEFAULT NULL, `id` INT AUTO_INCREMENT NOT NULL, `username` VARCHAR(100) NOT NULL, `password` VARCHAR(100) NOT NULL, `salt` VARCHAR(32) NOT NULL, `status` INT NOT NULL, `recovery_key` VARCHAR(64) DEFAULT NULL, `language` VARCHAR(5) NOT NULL, `timezone` VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_8D93D649854D4CE4 (`username`), INDEX IDX_8D93D6494D16C4DD (shop_id), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user_agent` (`uid` VARCHAR(64) NOT NULL, `data` JSON NOT NULL, PRIMARY KEY(`uid`)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `brand` ADD CONSTRAINT FK_1C52F9584D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `cart` ADD CONSTRAINT FK_BA388B79395C3F3 FOREIGN KEY (customer_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE `category` ADD CONSTRAINT FK_64C19C1C970124A FOREIGN KEY (main_media) REFERENCES `media` (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE `category` ADD CONSTRAINT FK_64C19C14D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `customer` ADD CONSTRAINT FK_81398E09A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `customer` ADD CONSTRAINT FK_81398E094D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `media` ADD CONSTRAINT FK_6A2CA10C4D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `media` ADD CONSTRAINT FK_6A2CA10CA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993984D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993989395C3F3 FOREIGN KEY (customer_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE `order_product` ADD CONSTRAINT FK_2530ADE68D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE `order_product` ADD CONSTRAINT FK_2530ADE6C970124A FOREIGN KEY (main_media) REFERENCES `media` (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE `order_product` ADD CONSTRAINT FK_2530ADE644F5D008 FOREIGN KEY (brand_id) REFERENCES `brand` (id)');
        $this->addSql('ALTER TABLE `product` ADD CONSTRAINT FK_D34A04AD4D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `product` ADD CONSTRAINT FK_D34A04AD727ACA70 FOREIGN KEY (parent_id) REFERENCES `product` (id)');
        $this->addSql('ALTER TABLE `product` ADD CONSTRAINT FK_D34A04ADC970124A FOREIGN KEY (main_media) REFERENCES `media` (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE `product` ADD CONSTRAINT FK_D34A04AD44F5D008 FOREIGN KEY (brand_id) REFERENCES `brand` (id)');
        $this->addSql('ALTER TABLE `product` ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES `category` (id)');
        $this->addSql('ALTER TABLE `product` ADD CONSTRAINT FK_D34A04ADBCB5C6F5 FOREIGN KEY (carts_id) REFERENCES `cart` (id)');
        $this->addSql('ALTER TABLE product_media ADD CONSTRAINT FK_CB70DA504584665A FOREIGN KEY (product_id) REFERENCES `product` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_media ADD CONSTRAINT FK_CB70DA50EA9FDD75 FOREIGN KEY (media_id) REFERENCES `media` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC73564584665A FOREIGN KEY (product_id) REFERENCES `product` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC735612469DE2 FOREIGN KEY (category_id) REFERENCES `category` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `setting` ADD CONSTRAINT FK_9F74B8984D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `shop` ADD CONSTRAINT FK_AC6A4CA2727ACA70 FOREIGN KEY (parent_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `task` ADD CONSTRAINT FK_527EDB254D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D6494D16C4DD FOREIGN KEY (shop_id) REFERENCES `shop` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order_product` DROP FOREIGN KEY FK_2530ADE644F5D008');
        $this->addSql('ALTER TABLE `product` DROP FOREIGN KEY FK_D34A04AD44F5D008');
        $this->addSql('ALTER TABLE `product` DROP FOREIGN KEY FK_D34A04ADBCB5C6F5');
        $this->addSql('ALTER TABLE `product` DROP FOREIGN KEY FK_D34A04AD12469DE2');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC735612469DE2');
        $this->addSql('ALTER TABLE `cart` DROP FOREIGN KEY FK_BA388B79395C3F3');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993989395C3F3');
        $this->addSql('ALTER TABLE `category` DROP FOREIGN KEY FK_64C19C1C970124A');
        $this->addSql('ALTER TABLE `order_product` DROP FOREIGN KEY FK_2530ADE6C970124A');
        $this->addSql('ALTER TABLE `product` DROP FOREIGN KEY FK_D34A04ADC970124A');
        $this->addSql('ALTER TABLE product_media DROP FOREIGN KEY FK_CB70DA50EA9FDD75');
        $this->addSql('ALTER TABLE `order_product` DROP FOREIGN KEY FK_2530ADE68D9F6D38');
        $this->addSql('ALTER TABLE `product` DROP FOREIGN KEY FK_D34A04AD727ACA70');
        $this->addSql('ALTER TABLE product_media DROP FOREIGN KEY FK_CB70DA504584665A');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC73564584665A');
        $this->addSql('ALTER TABLE `brand` DROP FOREIGN KEY FK_1C52F9584D16C4DD');
        $this->addSql('ALTER TABLE `category` DROP FOREIGN KEY FK_64C19C14D16C4DD');
        $this->addSql('ALTER TABLE `customer` DROP FOREIGN KEY FK_81398E094D16C4DD');
        $this->addSql('ALTER TABLE `media` DROP FOREIGN KEY FK_6A2CA10C4D16C4DD');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993984D16C4DD');
        $this->addSql('ALTER TABLE `product` DROP FOREIGN KEY FK_D34A04AD4D16C4DD');
        $this->addSql('ALTER TABLE `setting` DROP FOREIGN KEY FK_9F74B8984D16C4DD');
        $this->addSql('ALTER TABLE `shop` DROP FOREIGN KEY FK_AC6A4CA2727ACA70');
        $this->addSql('ALTER TABLE `task` DROP FOREIGN KEY FK_527EDB254D16C4DD');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6494D16C4DD');
        $this->addSql('ALTER TABLE `customer` DROP FOREIGN KEY FK_81398E09A76ED395');
        $this->addSql('ALTER TABLE `media` DROP FOREIGN KEY FK_6A2CA10CA76ED395');
        $this->addSql('DROP TABLE `brand`');
        $this->addSql('DROP TABLE `cart`');
        $this->addSql('DROP TABLE `category`');
        $this->addSql('DROP TABLE `customer`');
        $this->addSql('DROP TABLE `media`');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE `order_product`');
        $this->addSql('DROP TABLE `product`');
        $this->addSql('DROP TABLE product_media');
        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE `setting`');
        $this->addSql('DROP TABLE `shop`');
        $this->addSql('DROP TABLE `task`');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE `user_agent`');
    }
}
